# sixoclock 用户文档

## TODO

- [x] sixbox 教程更新
- [ ] 新API  下教程更新
- [ ] CWL中文教程 矫正


## 本地开发与部署
基于vuepress
https://v2.vuepress.vuejs.org/zh/reference/default-theme/config.html#logo


环境安装
```
cd six-docs
yarn install
```

快速启动
```
yarn docs:dev

```

## 分支说明

main分支为正式发布版

其余分支为开发分支，开发完请pull request

