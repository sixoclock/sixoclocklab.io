# 联系我们

* 扫描下方二维码，联系六点了官方客服
  
  ![客服二维码](../img/wechat_kefu_qrcode.png)

* 在线评论系统评论

	六点了支持用户通过我们在`gitter`上建立的讨论组讨论该项目的内容，如您有意反馈问题，请移步[6-oclock/community讨论组][1]

*  通过GitHub Issues提交反馈

	我们在GitHub仓库建立了关于该内容的issue板块，您可以将使用过程中遇到的问题反馈至[Bug-Tracker issue][2]。

*  通过邮箱联系我们

	六点了由六点了团队进行开发与维护，使用过程遇到的问题可以反馈至我们的技术支持邮箱：`info@sixoclock.net`

[1]: https://gitter.im/6-oclock/community
[2]: https://github.com/6-oclock/Bug-Tracker/issues