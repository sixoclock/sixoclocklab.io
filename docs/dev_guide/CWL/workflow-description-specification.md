# 工作流语法规范v1.0.2

## 1. 介绍
通用工作流语言 (CWL) 的工作组是一个非正式的多团队开发组，由对数据分析工作流的可移植性感兴趣的各种组织和个人组成。目标是创建一个通用规范，使数据科学家能够描述功能强大、易于使用、可移植并支持可重复性的分析工具和工作流程。

### 1.1 CWL工作流标准v1.0.2简介
这个规范代表了来自CWL团队的第三个稳定版本。从最初的1.0版本开始，v1.0.2对CWL命令行工具标准进行了一些更新。文档应该继续使用cwlVersion: v1.0，现有的v1.0文档仍然有效，但是依赖于以前未定义或未指定的行为的CWL文档在v1.0.2中可能有轻微的不同行为。 
 
* 12 March 2017:
  * Mark default as not required for link checking.
  * Add note that recursive subworkflows is not allowed.
  * Fix mistake in discussion of extracting field names from workflow step ids.
* 23 July 2017: (v1.0.1)
  * Add clarification about scattering over empty arrays.
  * Clarify interpretation of secondaryFiles on inputs.
  * Expanded discussion of semantics of File and Directory types
  * Fixed typo "EMACScript" to "ECMAScript"
  * Clarified application of input parameter default values when the input is null or undefined.
* 10 August 2017: (v1.0.2)
  * Clarify behavior resolving expressions in secondaryFile

* 从草案 3 开始，v1.0 对 CWL 工作流标准进行了以下更改和补充：

1. `inputs`和`outputs`已被重命名`in`和`out`。
2. 语法简化：由map<>语法表示。示例：`in` 包含一个项目列表，每个项目都有一个 `id`。现在可以指定该标识符到相应 `InputParameter`
3. 公共字段`description`已重命名为`doc`.


### 1.2 目的
通用工作流语言为数据密集型科学提供了工作流，例如生物信息学、化学、物理学和天文学。该规范旨在为工作流定义一个数据和执行模型，该模型可以在各种计算平台之上实现，包括单个工作站，集群、网格、云和高性能计算系统。

### 1.3 对其他规范的参考
Javascript Object Notation (JSON): http://json.org

JSON Linked Data (JSON-LD): http://json-ld.org

YAML: http://yaml.org

Avro: https://avro.apache.org/docs/1.8.1/spec.html

Uniform Resource Identifier (URI) Generic Syntax: https://tools.ietf.org/html/rfc3986)

Internationalized Resource Identifiers (IRIs): https://tools.ietf.org/html/rfc3987

Portable Operating System Interface (POSIX.1-2008): http://pubs.opengroup.org/onlinepubs/9699919799/

Resource Description Framework (RDF): http://www.w3.org/RDF/

### 1.4 适用范围
本文档描述了 CWL 语法、执行和对象模型。它不记录 CWL 特定的实现，但是它可以作为具体实现行为的参考。

### 1.5 术语
用于描述CWL文档的术语在规范的概念部分进行了定义。以下列表中定义的术语用于构建这些定义和描述 CWL 执行的操作：

**may**：允许符合 CWL 文档和 CWL 实现，但不要求其行为如所描述的那样。

**must**：要求符合 CWL 文档和 CWL 实现的行为如所述；否则他们是错误的。

**error**：违反本规范的规则；结果未定义。符合标准的操作可以检测和报告错误，并可以从中恢复。

**fatal error**：违反本规范的规则；结果未定义。符合要求的操作不能继续执行当前进程，并且可能会报错。

**at user option**：符合要求的软件可能或必须（取决于句子中的情态动词）按照描述的方式运行；如果是这样，它必须为用户提供一种启用或禁用所描述行为的方法。

**deprecated**：符合要求的软件可能会实现向后兼容的行为。便携式 CWL 文档不应依赖于已弃用的行为。标记为已弃用的行为可能会从 CWL 规范的未来修订版中完全删除。

## 2. 数据模型
### 2.1 数据概念
**object** 是数据结构等同于JSON“对象”类型，由无序集合的名称/值配对组合（这里称为字段），并且其中该名称是一个字符串，该值是一个字符串，数字，布尔、数组或对象。

**document** 是包含序列化对象，或对象的阵列的文件。

**process** 是计算的基本单元，其接受输入数据，执行一些计算，并且产生输出数据。示例包括 `CommandLineTools`、`Workflows` 和 `ExpressionTools`。

**input object** 描述流程调用的输入的对象。

**output object**描述调用流程所产生的输出的对象。

**input schema** 描述输入对象的有效格式(必需字段、数据类型)。

**output schema** 描述输出对象的有效格式

**Metadata** 是有关工作流、工具或输入项的信息。

### 2.2 语法
CWL 文档必须由使用 JSON 或 YAML 语法表示的对象或对象数组组成。加载时，CWL 实现必须应用链接[Semantic Annotations for Linked Avro Data (SALAD) Specification](https://www.commonwl.org/v1.0/SchemaSalad.html)中描述的预处理步骤 。可以使用[https://github.com/common-workflow-language/common-workflow-language/tree/master/v1.0](https://github.com/common-workflow-language/common-workflow-language/tree/master/v1.0)下的`SALAD schemas`验证 CWL的结构

### 2.3 标识符
如果对象包含一个`id`字段，则该字段用于唯一标识该文档中的对象。该id字段的值在整个文档中必须是唯一的。标识符可以相对于文档库和/或遵循[Schema Salad 规范](https://www.commonwl.org/v1.0/SchemaSalad.html#Identifier_resolution)中描述的规则的其他标识符进行解析 。

一个具体的实现操作可以选择只接受对id在本规范中明确列出其字段的对象类型的引用。

### 2.4 文件预处理
一个具体的实现操作必须解析[Schema Salad 规范](https://www.commonwl.org/v1.0/SchemaSalad.html)中描述的$import和$include指令。  

Schema salad 中定义的另一个变化是数据类型定义的简化。以?结尾的类型`<T>`应转换为`[<T>, "null"]`. 以`[]`结尾的类型`<T>`应转换为`{"type": "array", "items": <T>}`

### 2.5 扩展和元数据
输入`metadata`（例如，实验室样本标识符）可以使用显式传递到输出。本规范的未来版本可能会定义额外的工具来处理输入/输出元数据。

正确执行不需要的实现扩展（例如，与 GUI 表示相关的字段）和有关工具或工作流本身的元数据（例如，用于引用的作者身份）可以作为任何对象上的附加字段提供。此类扩展字段必须使用`$namespaces`文档部分中列出的命名空间前缀，如 [Schema Salad 规范](https://www.commonwl.org/v1.0/SchemaSalad.html#Explicit_context)中所述。

修改执行语义的实现扩展必须列在该`requirements`字段中。

## 3. 执行模型
### 3.1 执行概念
**parameter** 是一个输入或输出的命名符号，与相关联的数据类型或架构。在执行期间，将值分配给参数以使输入对象或输出对象用于具体流程调用。

**CommandLineTool** 是其特征在于，其上一些输入调用一个独立的，非交互式程序的执行过程中，产生输出，然后终止。

**workflow** 是由多个子步骤组成，其中步骤输出端连接到的下游步骤的输入，以形成有向无环图，和独立的步骤可以同时运行的过程。

**runtime environment** 执行的命令行工具时是实际的硬件和软件环境。它包括但不限于硬件架构、硬件资源、操作系统、软件运行时（如果适用，例如特定的 Python 解释器或特定的 Java 虚拟机）、库、模块、包、实用程序和数据文件需要运行该工具。

**workflow platform**  是一个能够解释CWL文件和执行由文件规定的程序的特定硬件和软件实现。工作流平台的职责可能包括调度流程调用、设置必要的运行环境、使输入数据可用、调用工具流程和收集输出。

旨在使工作流平台在本规范之外具有广泛的回旋余地，以优化计算资源的使用并执行本规范未涵盖的策略。当前不在 CWL 规范范围内但可由特定工作流平台处理的一些领域包括：

* 数据安全和权限  
* 在远程集群或云计算节点上调度工具调用。  
* 使用虚拟机或操作系统容器来管理运行时（DockerRequirement 中描述的除外）。
* 使用远程或分布式文件系统来管理输入和输出文件。
* 转换文件路径。
* 确定一个进程之前是否已执行，如果是，则跳过它并重用以前的结果。
* 暂停、恢复或检查点流程或工作流。  
除非通过使用流程要求明确声明，否则符合 CWL 流程不得假设任何有关运行时环境或工作流平台的内容。

### 3.2 通用执行过程
CWL 流程（包括工作流和命令行工具）的一般执行顺序如下。

1. 加载、处理和验证 CWL 文档，产生一个过程对象。
2. 加载输入对象。
3. 根据流程的`schema`验证输入`inputs`对象。
4. 验证通过。
5. 执行特定过程类型所需的任何进一步设置。
6. 执行流程。
7. 将流程执行的结果捕获到输出对象中。
8. 根据流程`schema`的验证输出`outputs`对象。
9. 将输出对象报告给进程调用者。

### 3.3 要求和提示
**process requirement** 要求修改的过程的语义或运行时环境。如果一个实现不能满足所有要求，或者列出了一个不被实现识别的需求，这是一个致命错误，实现不能尝试运行该过程，除非在用户选项中被覆盖。

**hint** 和 **requirement** 类似; 然而，如果一个实现不能满足所有`hints`，这不是错误。如果不能满足提示，则实现可能会报告警告。

**requirement** 是继承的。工作流程中指定的要求适用于所有工作流程步骤；在工作流步骤中指定的要求将适用于该步骤及其任何子步骤的流程实现。

如果相同的流程 **requirement** 出现在工作流的不同级别，则使用该 **requirement** 的最具体实例，即如果流程和步骤同时指定了requirement，则优先步骤，并且以hints相同的方式解析。

**Requirements** 覆盖提示。如果流程使用`hints`提供了一个流程requirement，同时`requirements`也指定了，则`requirements`优先。

### 3.4 参数引用
参数引用由语法$(…)表示，可以在任何本文档所指定的`Expression`字段中使用。参数引用使用以下[Javascript/ECMAScript 5.1](http://www.ecma-international.org/ecma-262/5.1/)语法子集，但它们被设计为不需要Javascript引擎进行计算。  
在接下来的BNF语法、字符类,语法规则用'`{}`'表示, '`-`'表示被排除,'`(())`'表示分组,“`|`”表示交替,带‘`*`’表示零个或多个重复，“`+`”表示一个或多个重复,‘`/`’非特殊字符,所有其他字符都是文字值。  
|                 |                      |  
|--------------------|----------------------------------------------|
| symbol:: | {Unicode alphanumeric}+                      |
| singleq::           | [' (( {character - '} \| \' ))* ']            |
| doubleq::             | [" (( {character - "} \| \" ))* "]            |
| index::                | [ {decimal digit}+ ]                         |
| segment::              | . {symbol} \| {singleq} \| {doubleq} \| {index} |
| parameter reference::  | $( {symbol} {segment}*)                      |

使用以下算法解析参数引用:
1. 将前导符号匹配为键
2. 在参数上下文中(如下所述)查找键以获得当前值。如果在参数上下文中没有找到键，则会出现错误。
3. 如果查找到结尾，终止并返回当前值
4. 否则，匹配下一部分
5. 从段中提取符号、字符串或索引作为键
6. 在当前值中查找键并将其赋值为新的当前值。如果键是符号或字符串，则当前值必须是对象。如果键是索引，则当前值必须是数组或字符串。如果键不匹配所需的类型，或者没有找到键或超出范围，则是错误的。
7. 重复步骤3 - 6

根名称空间是参数上下文。必须提供以下参数:
`inputs`:当前进程的输入对象。
`self`:上下文特定的值。“self”的上下文值在本规范的其他地方为特定字段提供了文档。如果一个字段的上下文值'self'没有被记录，它必须是'null'。
`runtime`:包含配置细节的对象。特定于流程类型。实现可以为运行时的任何或所有字段提供不透明的字符串。这些必须由平台在处理完工具之后，但在实际执行之前填写。参数引用和表达式只能使用字段的字符串值，除非另有说明，否则不得对内容执行计算。

如果字段的值在参数引用周围没有前导或尾随的非空格字符，则该字段的有效值将成为被引用参数的值，从而保留返回类型。
如果字段的值在参数引用周围有非空格的前导或尾随字符，则会受到字符串插值的影响。字段的有效值是一个包含前导字符、参数引用的字符串值和尾随字符的字符串。参数引用的字符串值是其文本JSON表示形式，规则如下:
* 从字符串中去掉前导和尾随引号
* 按键排序

多个参数引用可能出现在单个字段中。这种情况必须作为字符串插值处理。在插值第一个参数引用之后，必须递归地将插值应用于末尾字符，以产生最终的字符串值。

### 3.5 表达式
表达式是[Javascript/ECMAScript 5.1](http://www.ecma-international.org/ecma-262/5.1/)的代码规范，由通过工作流平台进行评估，以影响进程的输入、输出或行为。表达式不同于常规流程，因为它们旨在修改工作流本身的行为，而不是执行工作流的主要工作。  
为了声明表达式的使用，文档必须包含过程要求`inlinejavascriptrequrequirement`。本文中，表达式可以在任何允许伪类型表达式的字段中使用。

表达式由语法`$(…)`或`${…}`表示。包装在`$(…)`语法中的代码片段必须作为[ECMAScript expression](http://www.ecma-international.org/ecma-262/5.1/#sec-11)计算。对于匿名的零参数函数，代码片段包装在`${…}`语法必须作为[ECMAScript function body](http://www.ecma-international.org/ecma-262/5.1/#sec-13)计算。表达式必须返回一个有效的JSON数据类型:空、字符串、数字、布尔值、数组、对象之一。其他返回值必然导致`permanentFailure`。实现必须允许任何语法上有效的 `Javascript`，并说明圆括号或大括号的嵌套，以及可能包含圆括号或大括号的字符串。  
在执行实际的表达式之前，runtime必须包含在 ["expressionLib" field of InlineJavascriptRequirement](https://www.commonwl.org/v1.0/Workflow.html#InlineJavascriptRequirement)字段中定义的任何代码。  
在执行表达式之前，runtime必须将上述参数上下文的字段初始化为全局变量。
表达式求值后字段的有效值遵循与上面讨论的参数引用相同的规则。多个表达式可能出现在一个字段中。  
表达式的求值顺序是未定义的，除非在本文档中另有说明。
一个具体实施可以选择通过作为Javascript表达式求值来实现参数引用。求值参数引用的结果必须是相同的，无论用Javascript求值还是其他方法实现。  
一个具体实施可能会应用其他限制，如进程隔离、超时和操作系统容器/监禁，以最小化与运行嵌入在CWL文档中的不受信任代码相关的安全风险。  
从表达式抛出的异常必然导致进程的`permanentFailure`。

### 3.6 将 CWL 文档作为脚本执行
按照惯例，CWL文档可以以`#!/usr/bin/env cwl-runner`开头，并标记为可执行文件（POSIX“+x”权限位）以使其能够直接执行。工作流平台可能支持这种操作模式；如果是这样，它必须为`cwl-runner`平台的 CWL 实现提供别名。

CWL 输入对象文档可以类似地`#!/usr/bin/env cwl-runner`以可执行文件开头并被标记为可执行文件。在这种情况下，输入对象必须包括`cwl:tool`向默认 CWL 文档提供 IRI 的字段，该文档应该使用输入对象的字段作为输入参数来执行

### 3.7 查找本地文件系统上CWL文档
要发现 CWL 文档，请查看以下位置：

/usr/share/commonwl/

/usr/local/share/commonwl/

\$XDG_DATA_HOME/commonwl/（通常$HOME/.local/share/commonwl）

\$XDG_DATA_HOME来自[ XDG Base Directory Specification](http://standards.freedesktop.org/basedir-spec/basedir-spec-0.6.html)

## 4. 工作流
工作流描述了一组步骤以及这些步骤之间的依赖关系。当一个步骤产生的输出将被第二步使用时，第一步是第二步的依赖。

当存在依赖时，工作流引擎必须执行前面的步骤并等待它成功产生输出，然后再执行依赖步骤。如果在工作流图中定义了两个不直接或间接依赖的步骤，则这些步骤是独立的，可以按任意顺序执行或并发执行。执行完所有步骤后，工作流就完成了。

使用工作流步骤输入参数和工作流输出参数`source`上的字段 表示参数之间的相关性。

该`source`字段表示一个参数对另一个参数的依赖性，这样当一个值与由 指定的参数相关联时 `source`，该值将传播到目标参数。当进入给定步骤的所有数据链接都完成后，该步骤就可以执行了。

#### 工作流成功与失败
完成的步骤必然会是success、temporaryFailure或 permanentFailure状态之一。若为temporaryFailure，可以选择重试。permanentFailure状态时，可以选择继续运行工作流的其他步骤，或者立即终止。

如果工作流执行的任何步骤导致permanentFailure，则工作流状态为permanentFailure。

如果一个或多个步骤导致temporaryFailure并且所有其他步骤完成success或未执行，则工作流状态为 temporaryFailure。

如果所有工作流步骤均已执行并以 完成success，则工作流状态为success。

#### 拓展
[ScatterFeatureRequirement](https://www.commonwl.org/v1.0/Workflow.html#ScatterFeatureRequirement) 和 [SubworkflowFeatureRequirement](https://www.commonwl.org/v1.0/Workflow.html#SubworkflowFeatureRequirement) 用作核心工作流语义的标准扩展。  

#### 工作流包含的字段 
| field        | required | type       | description                                                   |
|--------------|----------|------------|---------------------------------------------------------------|
| inputs       | required | array,map  | 定义流程的输入参数                                                     |
| outputs      | required | array,map  | 定义流程的；可用于生成和/或验证输出对象                                          |
| class        | required | string     |  -                                                            |
| steps        | required | array,map  | 组成工作流的各个步骤                                                    |
| id           | optional | string     | 此进程对象的唯一标识符                                                   |
| requirements | optional | array,map  | 声明应用于运行时环境或工作流引擎的需求，为执行此流程必须满足这些需求                            |
| hints        | optional | array,map  | 声明应用于运行时环境或工作流引擎的提示，这些提示可能有助于执行此过程。如果不能满足所有提示，则不是错误，但是可能报告警告。 |
| label        | optional | string     | 简短、人类可读的标签。                                                   |
| doc          | optional | string     | 长且易读的描述                                                       |
| cwlversion   | optional | CWLVersion | CWL文档版本                                                       |

### 4.1 WorkflowOutputParameter
描述工作流的输出参数。参数必须连接到工作流中定义的一个或多个参数，这些参数将提供输出参数的值。
| field          | required | type                                                                                | description                                              |
|----------------|----------|-------------------------------------------------------------------------------------|----------------------------------------------------------|
| id             | required | string                                                                              | 此参数对象的唯一标识符。                                             |
| label          | optional | string                                                                              | 短的、人类可读的标签。                                              |
| secondaryFiles | optional | string,array                                                                        | 提供一个模式或表达式，指定必须包含在主文件旁边的文件或目录.                           |
| streamable     | optional | boolean                                                                             | true表示按顺序读写文件，不进行查找。可以使用此标志来指示使用指定管道流文件内容是否有效。默认值:false。 |
| doc            | optional | string,array                                                                        | 长且易读的描述                                                  |
| outputBinding  | optional | CommandOutputBinding                                                                | 描述如何处理流程的输出。                                             |
| format         | optional | string,Expression                                                                   | 这是将分配给输出参数的文件格式。                                         |
| outputSource   | optional | string,array                                                                        | 指定一个或多个工作流参数，为输出参数提供值。                                   |
| linkMerge      | optional | LinkMergeMethod                                                                     | 将多个源合并为单个数组的方法。如果未指定，默认方法为“merge_nested”。                |
| type           | optional | CWLType , OutputRecordSchema , OutputEnumSchema ,OutputArraySchema , string . array | 指定可分配给此参数的有效数据类型                                         |

#### 4.1.1 Expression 
"**Expression**" 不是真正的类型。它表示字段必须允许运行时参数引用。如果平台 声明并支持InlineJavascriptRequirement，则该字段还必须允许 Javascript 表达式。  
| symbol          | description 
|----------------|----------|
| ExpressionPlaceholder           


### 4.1.2 CommandOutputBinding
描述如何根据CommandLineTool生成的文件生成输出参数。  
输出参数的值依次为:  
* glob  
* loadContents  
* outputEval  
* secondaryFiles      

| field        | required | type                    | description                                                                                                                                                |
|--------------|----------|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| glob         | optional | string,Expression,array | 查找相对于输出目录的文件,如果提供了数组，则查找匹配数组中任何模式的文件。如果提供了表达式，则该表达式必须返回一个字符串或字符串数组，然后将作为一个或多个glob模式进行计算。必须只匹配和返回实际存在的文件。                                                   |
| loadContents | optional | boolean                 | 对于glob中匹配的每个文件，从文件中读取到前64 KiB文本，并将其放在文件对象的contents字段中，以便由outputEval进行操作。                                                                                   |
| outputEval   | optional | string,Expression       | 计算表达式以生成输出值。如果指定了glob，则self的值必须是一个包含匹配的文件对象的数组。如果没有匹配的文件，self必须是零长度的数组;如果匹配的是单个文件，则self的值是单个元素的数组。此外，如果loadContents为真值，File对象必须在contents字段中包含前64 KiB的文件内容。 |


### 4.1.3 LinkMergeMethod 
输入link merge方法，在WorkflowStepInput中描述。

| symbol          | description 
|----------------|----------|
| merge_nested	
| merge_flattened
### 4.1.4 CWLType 
通过将文件和目录作为内置类型的概念扩展基本类型。
| symbol    | description         |
|-----------|---------------------|
| null      | 空值                  |
| boolean   | 布尔值                 |
| int       | 32位符号整型             |
| long      | 64位符号整型             |
| float     | 单精度(32位)IEEE 754浮点数 |
| double    | 双精度(64位)IEEE 754浮点数 |
| string    | Unicode字符序列         |
| File      | 文件                  |
| Directory | 目录                  |

### 4.1.5 File 
`File`表示一个文件（或secondaryFiles提供时的一组文件），工具可以使用标准 POSIX 文件系统调用 API（例如 open(2) 和 read(2)）访问该文件  
Files 表示为带有class的对象File。文件对象具有许多提供有关文件的元数据的属性。  
`File`的`location` 属性是文件的统一资源标识符(URI), 支持本地file:// URI 或者 http://， 也可以是相对路径。除location, path 属性也是File可接受的，它必须是与CWL运行程序(用于输入)或命令行工具执行的运行时环境(用于命令行工具输出)相同的主机上可用的文件系统路径。  
如果没有指定`location` 或 `path` 则文件对象必须使用文件的UTF-8文本内容指定内容`contents`，`contents`的最大大小为64 kb。  
`secondaryFiles`属性是必须在与主文件相同的目录中暂存的文件或目录对象的列表。在`secondaryFiles`中复制文件名是一个错误。  
`size`属性是以字节为单位的文件大小。它必须从资源中计算并提供给表达式。`checksum`字段包含文件内容的加密hash，用于验证文件内容。  
当收集`CommandLineTool`输出时，`glob`匹配返回文件路径(带有`path`属性)和派生属性。这些都可以通过`outputEval`进行修改。或者，如果输出中出现了`cwl.output.json`文件，则忽略`outputBinding`。  
输出中的文件对象必须在工具执行运行时的上下文中(在计算节点本地，或在执行容器内)提供`location`  URI或`path`属性。  

| field          | required | type                | description                                          |
|----------------|----------|---------------------|------------------------------------------------------|
| class          | required | constant value File | File                                                 |
| location       | optional | string              | 标识文件资源的URI。                                          |
| path           | optional | string              | 当执行CommandLineTool时，文件可用的本地主机路径                      |
| basename       | optional | string              | 文件的基本名称                                              |
| dirname        | optional | string              | 包含文件的目录名                                             |
| nameroot       | optional | string              | nameroot + nameext == basename                       |
| nameext        | optional | string              | nameroot + nameext == basename                       |
| checksum       | optional | string              | 用于验证文件完整性的可选hash代码,当前必须以"sha1$ +十六进制字符串"的形式使用SHA-1算法 |
| size           | optional | string              | 可选的文件大小                                              |
| secondaryFiles | optional | array               | 与主文件关联并必须与主文件一起传输的附加文件或目录的列表                         |
| format         | optional | string              | 文件的格式                                                |
| contents       | optional | string              | 文件内容。最大64kib。                                        |

#### 4.1.5.1 Directory 
表示要呈现给命令行工具的目录,`Directory`表示为具有Directory类的对象。目录对象有许多属性，它们提供关于目录的元数据.  
`Directory`对象可能有一个`listing`字段。这是包含在Directory中的`File`和`Directory`对象的列表。对于清单中的每个条目，`basename`属性定义暂存到磁盘时的`File`或`Subdirectory`的名称。如果没有提供清单，则实现必须有某种方法在运行时基于位置字段获取Directory清单  
CommandLineTool输出中的`Directory`对象必须提供一个位置URI或工具执行运行时上下文中的路径属性(在计算节点本地，或在执行容器内)。  
名称冲突(相同的`basename`在清单中多次出现，或者在清单中的`secondaryFiles`中的任何条目中多次出现)是一个致命错误。  
| field    | required | type                | description                     |
|----------|----------|---------------------|---------------------------------|
| class    | required | constant value File | 标识这是Directory                   |
| location | optional | string              | 标识文件资源的URI。                     |
| path     | optional | string              | 当执行CommandLineTool时，文件可用的本地主机路径 |
| basename | optional | string              | 文件的基本名称                         |
| listing  | optional | array               | 此目录中包含的文件或子目录的列表                |

### 4.1.6 OutputRecordSchema 
| field  | required | type                  | description  |
|--------|----------|-----------------------|--------------|
| type   | required | constant value record | 必须为 record   |
| fields | optional | array                 | 定义record的字段。 |
| label  | optional | string                | 短的、人类可读的标签   |

### 4.1.7 OutputRecordField 
| field         | required | type                                                                                | description |
|---------------|----------|-------------------------------------------------------------------------------------|-------------|
| name          | required | string                                                                              | 字段名字        |
| type          | required | CWLType , OutputRecordSchema , OutputEnumSchema ,OutputArraySchema , string . array | 类型          |
| doc           | optional | string                                                                              | 此字段的说明文档    |
| outputBinding | optional | CommandOutputBinding                                                                |             |

#### 4.1.7.1 OutputEnumSchema
| field         | required | type                 | description |
|---------------|----------|----------------------|-------------|
| symbols       | required | string               | 定义有效符号集。    |
| type          | required | constant value enum  | 必须为enum     |
| label         | optional | string               | 简短的，人类可读的标签 |
| outputBinding | optional | CommandOutputBinding |             |

#### 4.1.7.2 OutputArraySchema
| field         | required | type                                                                                | description |
|---------------|----------|-------------------------------------------------------------------------------------|-------------|
| items         | required | CWLType , OutputRecordSchema , OutputEnumSchema ,OutputArraySchema , string . array | 定义数组元素的类型   |
| type          | required | constant value enum                                                                 | 必须为array    |
| label         | optional | string                                                                              | 简短的，人类可读的标签 |
| outputBinding | optional | CommandOutputBinding                                                                |             |


### 4.2 WorkflowStep
`WorkflowStep` 是工作流的可执行元素。它在`run`字段中指定底层流程实现（例如`CommandLineTool`或其他 `Workflow`），并将底层流程的输入和输出参数连接到工作流参数。
#### Scatter/gather 
要使用`Scatter/gather`， 必须在工作流或工作流步骤要求中指定`ScatterFeatureRequirement`。

“Scatter”操作指定关联的工作流步骤或子工作流应在输入元素列表上单独执行。组成Scatter操作的每个作业都是独立的，可以并发执行。

该`scatter`字段指定一个或多个将被分散的输入参数。一个输入参数可能会被多次列出。每个输入参数的声明类型隐式地成为输入参数类型的项的数组。如果一个参数被多次列出，它就会变成一个嵌套数组。因此，连接到分散参数的上游参数必须是数组。

所有输出参数类型也隐式包装在数组中。Scatter中的每个作业都会在输出数组中生成一个条目。

如果任何Scatter参数运行时值是空数组，则根据适用的Scatter规则，所有输出都设置为空数组并且不为该步骤做任何工作。

如果`scatter`声明了多个输入参数，则`scatterMethod` 描述如何将输入分解为一组离散的作业。

* **dotproduct** 指定每个输入数组都是对齐的，并且从每个数组中取出一个元素来构造每个作业。如果所有输入数组的长度不同，则会出错。

* **nested_crossproduct** 指定输入的笛卡尔积，为分散输入的每个组合生成一个作业。输出必须是每个散射级别的嵌套数组，按照输入数组在scatter字段中列出的顺序。

* **flat_crossproduct** 指定输入的笛卡尔积，为分散输入的每个组合生成一个作业。输出数组必须展平为单个级别，否则按输入数组在scatter字段中列出的顺序列出。
#### Subworkflows 
要将嵌套工作流指定为工作流步骤的一部分， 必须在工作流或工作流步骤要求中指定SubworkflowFeatureRequirement。  
如果工作流直接或间接调用自身作为子工作流（不允许递归工作流），则是致命错误。
| field         | required | type                                                 | description                            |
|---------------|----------|------------------------------------------------------|----------------------------------------|
| id            | required | string                                               | 此工作流步骤的唯一标识符。                          |
| in            | required | array,map                                            | 定义工作流步骤的输入参数                           |
| out           | required | array                                                | 定义表示流程输出的参数。                           |
| run           | required | string , CommandLineTool , ExpressionTool , Workflow | 指定要运行的进程。                              |
| requirements  | optional | array,map                                            | 声明应用于运行时环境或工作流引擎的需求，执行此工作流步骤必须满足这些需求   |
| hints         | optional | array,map                                            | 声明应用于运行时环境或工作流引擎的提示，这些提示可能有助于执行此工作流步骤。 |
| label         | optional | string                                               | 简短的，人类可读的标签                            |
| doc           | optional | string                                               | 长且易读的描述。                               |
| scatter       | optional | string,array                                         |  -                                     |
| scatterMethod | optional | ScatterMethod                                        | 如果scatter是一个包含多个元素的数组，则必选。             |


### 4.2.1 WorkflowStepInput 
工作流步骤的输入将上游参数（来自工作流输入或其他工作流步骤的输出）与基础步骤的输入参数连接起来。

Input object
`WorkflowStepInput` 对象必须包含`id`字段，格式为 `#fieldname`或 `#prefix/fieldname`。当`id`字段包含斜杠时`/`，字段名称由最后一个斜杠后面的字符组成（前缀部分可能包含一个或多个斜杠以指示范围）。这定义了具有source参数值的工作流步骤输入对象的字段。

Merging   
要合并多个入数据链接， 必须在工作流或工作流步骤要求中指定`MultipleInputFeatureRequirement`。

如果接收器参数是数组，或者在工作流分散操作中命名，则该source字段中可能会列出多个入数据链接。输入链接中的值将根据linkMerge字段中指定的方法进行合并。如果未指定，则默认方法为`“merge_nested”`。

* merge_nested  
  
    输入必须是一个数组，每个输入链接只包含一个条目。如果使用单个链接指定“merge_nested”，则链接中的值必须包含在单项列表中。

* merge_flattened  
  1. `source` 和 `sink` 参数必须是兼容的类型，或者源类型必须与目标数组参数的“items”类型中的单个元素兼容。
  2. 作为数组的源参数被连接起来。作为单个元素类型的源参数被附加为单个元素。

| field     | required | type                | description                                                                                                                                      |
|-----------|----------|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| id        | required | string              | 此工作流输入参数的唯一标识符。                                                                                                                                  |
| source    | optional | string,array        | 指定一个或多个工作流参数，这些参数将为底层步骤参数提供输入。                                                                                                                   |
| linkMerge | optional | LinkMergeMethod     | 用于将multiple inbound links 合并到单个数组中的方法。如果未指定，默认方法为“merge_nested”。                                                                                 |
| default   | optional | Any                 | 如果没有source字段，或者source生成的值为空，则使用此参数的默认值                                                                                                           |
| valueFrom | optional | string , Expression | 要使用valueFrom, StepInputExpressionRequirement必须在工作流或工作流步骤需求中指定。如果valueFrom是常量字符串值，则使用this作为输入参数的值。如果valueFrom是参数引用或表达式，则必须对其进行计算，以产生要分配给输入字段的实际值。 |

#### 4.2.1.1 Any 
`Any`类型表示任何非空值.
### 4.2.2 WorkflowStepOutput 
将底层流程的输出参数与工作流参数相关联。工作流参数（在id字段中给出）可以用作source连接其他工作流步骤的输入参数，或连接过程的输出参数。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| id    | required | string | 此工作流输出参数的唯一标识符。这是WorkflowStepInput的source字段中用于将输出值连接到下游参数的标识符。 |

### 4.2.3 ScatterMethod 
scatter 方法, 在4.2 workflow step scatter中描述。
| symbol          | description 
|----------------|----------|
| dotproduct	
| nested_crossproduct
| flat_crossproduct

### 4.2.4 InlineJavascriptRequirement 
表示工作流平台必须支持内联 Javascript 表达式。如果此要求不存在，则工作流平台不得执行表达式插值。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 必为InlineJavascriptRequirement
| expressionLib| optional | array| 在执行表达式代码之前，还将插入其他代码片段。允许从CWL表达式中调用函数定义。

### 4.2.5 SchemaDefRequirement 
该字段由类型定义数组组成，在解释`inputs`和`outputs`字段时必须使用这些类型定义。当`type`字段包含 IRI 时，实现必须检查类型是否在 `schemaDefs`其中定义并使用该定义。如果在 中找不到类型 `schemaDefs`，则是错误。`schemaDefs`必须按照列出的顺序处理条目，以便后面的模式定义可以引用早期的模式定义
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 为SchemaDefRequirement
| types | required | array | 类型定义列表

#### 4.2.5.1 InputRecordSchema
| field  | required | type                  | description |
|--------|----------|-----------------------|-------------|
| type   | required | constant value record | 必须为record   |
| fields | optional | array                 | 定义record的字段 |
| label  | optional | string                | 短的、人类可读的标签。 |
| name   | optional | string                | 字段名         |

#### 4.2.5.2 InputRecordField
| field        | required | type                                                                              | description |
|--------------|----------|-----------------------------------------------------------------------------------|-------------|
| name         | required | string                                                                            | 字段名         |
| type         | required | CWLType , InputRecordSchema , InputEnumSchema , InputArraySchema , string , array | 类型          |
| doc          | optional | string                                                                            | 字段的文档       |
| inputBinding | optional | CommandLineBinding                                                                |             |
| label        | optional | string                                                                            | 短的、人类可读的标签。 |

#### 4.2.5.2.1 InputEnumSchema
| field         | required | type                 | description |
|---------------|----------|----------------------|-------------|
| symbols       | required | string               | 定义有效符号集。    |
| type          | required | constant value enum  | 必须为enum     |
| label         | optional | string               | 简短的，人类可读的标签 |
| name          | optional | string               |             |
| outputBinding | optional | CommandOutputBinding |             |

#### 4.2.5.2.2 CommandLineBinding
| field         | required | type               | description                                                                                                                       |
|---------------|----------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| loadContents  | optional | boolean            | 仅当type: File or items: File.时有效                                                                                                   |
| position      | optional | int                | 排序键。默认位置为0。                                                                                                                       |
| prefix        | optional | string             | 要添加到值之前的命令行前缀。                                                                                                                    |
| separate      | optional | boolean            | 如果为true(默认)，那么前缀和值必须作为单独的命令行参数添加;如果为false，前缀和值必须连接到单个命令行参数中。                                                                      |
| itemSeparator | optional | string             | 将数组元素连接成一个由itemSeparator分隔的元素的字符串。                                                                                                |
| valueFrom     | optional | string, Expression | 如果valueFrom是常量字符串值，则使用此值并应用上面的绑定规则。如果valueFrom是一个表达式，请计算该表达式以生成用于构建命令行的实际值，并应用上面的绑定规则。                                            |
| shellQuote    | optional | boolean            | 如果shellcommandrequimand在当前命令的需求中，这将控制是否在命令行中引用该值(默认为true). 如果shellQuote为true或未提供，则必须不允许解释任何shell元字符或指令。 使用shellQuote: false 加入通配符 |

#### 4.2.5.2.3 InputArraySchema
| field        | required | type                                                                            | description |
|--------------|----------|---------------------------------------------------------------------------------|-------------|
| items        | required | CWLType ,InputRecordSchema , InputEnumSchema ,InputArraySchema , string | array | 定义数组元素的类型。  |
| type         | required | constant value array                                                            | 必须为array    |
| label        | optional | string                                                                          | 简短的，人类可读的标签 |
| inputBinding | optional | CommandLineBinding                                                              |             |

### 4.2.6 SoftwareRequirement 
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | SoftwareRequirement
| packages | required | array| 需要配置的软件列表。

### 4.2.7 SoftwarePackage 
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| package	| required | string | 要提供的软件的名称。如果名称是通用的、不一致的或不明确的，则应该在spec字段中与一个或多个标识符组合。
| version | optional | array| 已知可兼容的软件(可选)版本。
| specs | optional | array| 软件说明

### 4.2.8 InitialWorkDirRequirement 
在执行命令行工具之前，在指定的输出目录中定义工作流平台必须创建的文件和子目录列表
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | InitialWorkDirRequirement
| listing | required | array| 在执行命令行工具之前，必须放在指定输出目录中的文件或子目录的列表。

#### 4.2.8.1 Dirent
定义在执行命令行工具之前必须放在指定输出目录中的文件或子目录。可能是执行表达式的结果，例如从模板构建配置文件。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| entry	| required | string,Expression | 如果值是字符串字面值或计算为字符串的表达式，则必须创建一个新文件，并将该字符串作为文件内容。如果该值是一个计算结果为File对象的表达式，则表示应该在执行工具之前将引用的文件添加到指定的输出目录中。如果该值是一个计算结果为Dirent对象的表达式，这表明应该将entry中的File or Directory添加到指定的输出目录中，其名称位于entryname中。如果writable为false，则可以使用绑定挂载或文件系统链接使该文件可用，以避免不必要地复制输入文件。
| entryname | optional |string , Expression | 要在输出目录中创建的文件或子目录的名称。如果条目是File或Directory，则entryname字段将覆盖File或Directory对象的basename值。可选的。
| writeable | optional | boolean| 如果为true，则文件或目录必须是工具可写的。对文件或目录的更改必须是隔离的，并且不能被任何其他CommandLineTool进程看到。这可以通过复制原始文件或目录来实现。默认false(文件和目录默认为只读)。标记为可写的目录:true意味着所有文件和子目录也是递归可写的。

### 4.2.9 SubworkflowFeatureRequirement 
表示工作流平台必须支持WorkflowSteprun字段中的嵌套工作流。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 总是为SubworkflowFeatureRequirement

### 4.2.10 ScatterFeatureRequirement
表示工作流平台必须支持WorkflowStep的scatter和 scatterMethod字段。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 总是为ScatterFeatureRequirement

### 4.2.11 MultipleInputFeatureRequirement 
表示工作流平台必须支持WorkflowStepInputsource字段中列出的多个入数据链接。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 总是为MultipleInputFeatureRequirement
### 4.2.12 StepInputExpressionRequirement 
表示工作流平台必须支持WorkflowStepInputvalueFrom字段。
| field | required | type   | description                                                    |
|-------|----------|--------|----------------------------------------------------------------|
| class	| required | string | 总是为StepInputExpressionRequirement
### 4.2.13 ExpressionTool 
将表达式作为工作流步骤执行。
| field        | required | type                                     | description                            |
|--------------|----------|------------------------------------------|----------------------------------------|
| inputs       | required | array,map                                | 定义输入参数                                 |
| outputs      | required | array,map,ExpressionToolOutputParameter> | 定义表示流程输出的参数。可用于生成和/或验证输出对象。            |
| class        | required | string                                   |                                        |
| expression   | required | string , Expression                      | 要执行的表达式。                               |
| id           | optional | string                                   | 进程对象的唯一标识符。                            |
| requirements | optional | array                                    | 声明应用于运行时环境或工作流引擎的需求，执行此工作流步骤必须满足这些需求   |
| hints        | optional | array                                    | 声明应用于运行时环境或工作流引擎的提示，这些提示可能有助于执行此工作流步骤。 |
| label        | optional | string                                   | 长且易读的描述。                               |
| doc          | optional | string                                   | 简短的，人类可读的标签                            |
| cwlVersion   | optional | CWLVersion                               | CWL文档版本                                |

#### 4.2.13.1 InputParameter
| field          | required | type                                                                                | description                                              |
|----------------|----------|-------------------------------------------------------------------------------------|----------------------------------------------------------|
| id             | required | string                                                                              | 此参数对象的唯一标识符。                                             |
| label          | optional | string                                                                              | 短的、人类可读的标签。                                              |
| secondaryFiles | optional | string,array                                                                        | 提供一个模式或表达式，指定必须包含在主文件旁边的文件或目录.                           |
| streamable     | optional | boolean                                                                             | true表示按顺序读写文件，不进行查找。可以使用此标志来指示使用指定管道流文件内容是否有效。默认值:false。 |
| doc            | optional | string,array                                                                        | 长且易读的描述                                                  |
| format         | optional | string,Expression                                                                   | 这是将分配给输出参数的文件格式。                                         |
| inputBinding   | optional | CommandLineBinding                                                                  | 描述如何处理流程的输入，并将其转换为具体的表单以供执行，例如命令行参数。                     |
| default        | optional | Any                                                                                 | 如果输入对象中缺少参数，或者输入对象中的参数值为空，则此参数使用的默认值                     |
| type           | optional | CWLType , IntputRecordSchema , IntputEnumSchema ,IntputArraySchema , string . Array | 指定可分配给此参数的有效数据类型                                         |


#### 4.2.13.2 ExpressionToolOutputParameter 
| field          | required | type                                                                                | description                                              |
|----------------|----------|-------------------------------------------------------------------------------------|----------------------------------------------------------|
| id             | required | string                                                                              | 此参数对象的唯一标识符。                                             |
| label          | optional | string                                                                              | 短的、人类可读的标签。                                              |
| secondaryFiles | optional | string,array                                                                        | 提供一个模式或表达式，指定必须包含在主文件旁边的文件或目录.                           |
| streamable     | optional | boolean                                                                             | true表示按顺序读写文件，不进行查找。可以使用此标志来指示使用指定管道流文件内容是否有效。默认值:false。 |
| doc            | optional | string,array                                                                        | 长且易读的描述                                                  |
| outputBinding  | optional | CommandOutputBinding                                                                | 描述如何处理流程的输出。                                             |
| format         | optional | string,Expression                                                                   | 这是将分配给输出参数的文件格式。                                         |
| type           | optional | CWLType , IntputRecordSchema , IntputEnumSchema ,IntputArraySchema , string . Array | 指定可分配给此参数的有效数据类型                                         |

#### 4.2.13.3 CWLVersion
发布的CWL文档版本的版本符号。

| symbol       | description |
|--------------|-------------|
| draft-2      |             |
| draft-3.dev1 |             |
| draft-3.dev2 |             |
| draft-3.dev3 |             |
| draft-3.dev4 |             |
| draft-3.dev5 |             |
| draft-3      |             |
| draft-4.dev1 |             |
| draft-4.dev2 |             |
| draft-4.dev3 |             |
| v1.0.dev4    |             |
| v1.0         |             |
