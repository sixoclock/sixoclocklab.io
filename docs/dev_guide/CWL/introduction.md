# 介绍


CWL是一种描述命令行工具的方式，并将它们连接在一起以创建工作流程。由于CWL是一个规范，而不是一个具体的软件，使用CWL描述的工具和工作流程可以在支持CWL标准的各种平台上移植。

CWL起源于 "make "和许多类似的工具，它们根据任务之间的依赖关系来决定执行顺序。然而，与 "make "不同，CWL的任务是独立的，你必须明确你的输入和输出。明确和独立的好处是灵活、可移植和可扩展：用CWL描述的工具和工作流可以利用Docker等技术，并与不同来源的CWL实现一起使用。CWL非常适用于描述集群、云和高性能计算环境中的大规模工作流，这些环境中的任务是在许多节点上并行调度的。

## 简介：

官网：[https://www.commonwl.org/](https://www.commonwl.org/)

github：[https://github.com/common-workflow-language/common-workflow-language](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fcommon-workflow-language%2Fcommon-workflow-language)

## 现有的cwl流程：

1）[__https://github.com/truwl/capanno/tree/master/cwl-tools__](http://www.sixoclock.net/application/pipes)

2）[__https://github.com/common-workflow-library/bio-cwl-tools__](https://github.com/common-workflow-library/bio-cwl-tools)

3）[__https://github.com/common-workflow-library/legacy/tree/master/tools__](https://github.com/common-workflow-library/legacy/tree/master/tools)

## 安装 Sixbox以运行CWL Workflow

1. 前往[下载中心](https://www.sixoclock.net/download-center)下载安装脚本[Sixbox_linux64_latest.sh](https://www.sixoclock.net/resources/dist/latest/Sixbox_linux64_latest.sh)

1. 在linux终端运行以下指令

```text
bash Sixbox_linux64_latest.sh
```

3. 按照安装程序屏幕上的提示进行操作（如果您不确定任何设置，可以接受默认值）。

4. 为了使更改生效，请关闭然后重新打开终端窗口。

5. 测试您的安装。在终端窗口，运行`sixbox -h`命令。如果已正确安装，将显示参数说明。

至此，`sixbox-linux`的安装和配置顺利结束。您可以使用`sixbox`运行`CWL Workflow`

sixbox运行`CWL Workflow`示例：

```text
sixbox run ./samtools-mpileup.cwl ./samtools-mpileup.yml
```

其中./samtools-mpileup.cwl定义了软件的参数结构与输入输出，./samtools-mpileup.yml定义了软件参数的值与输入文件