# 命令行语法规范v1.0.2


## 摘要

命令行工具是一个非交互式可执行程序，读取一些输入，执行计算，并在产生一些输出后终止。命令行程序是一个灵活的代码共享和重用单元，不幸的是，命令行程序之间的语法和输入/输出语义极其不同。用于描述程序语法和语义的公共层可以通过提供将程序连接在一起的一致方式来减少这种附带复杂性。本规范定义了通用工作流语言（CWL）命令行工具描述，这是一个中立的标准，用于描述命令行程序的语法和输入/输出语义。

## 本文件的状态

本文档是[通用工作流程语言工作组](https://groups.google.com/forum/#!forum/common-workflow-language)的产物。本文档的最新版本可在“v1.0”目录中找到，网址为[https://github.com/common-workflow-language/common-workflow-language](https://github.com/common-workflow-language/common-workflow-language)

CWL工作组的产品（包括本文件）根据Apache许可证2.0版的条款提供。

# 目录

[通用工作流程语言（CWL）命令行工具描述，v1.0.2](https://www.commonwl.org/v1.0/CommandLineTool.html#Common_Workflow_Language_(CWL)_Command_Line_Tool_Description,_v1.0.2)

[摘要](https://www.commonwl.org/v1.0/CommandLineTool.html#Abstract)

[本文件的状态](https://www.commonwl.org/v1.0/CommandLineTool.html#Status_of_this_document)

[1.导言](https://www.commonwl.org/v1.0/CommandLineTool.html#Introduction)

  [1.1 CWL命令行工具标准v1.0.2简介](https://www.commonwl.org/v1.0/CommandLineTool.html#Introduction_to_CWL_Command_Line_Tool_standard_v1.0.2)

  [1.2目的](https://www.commonwl.org/v1.0/CommandLineTool.html#Purpose)

  [1.3 其他规范](https://www.commonwl.org/v1.0/CommandLineTool.html#Dirent)

  [1.4范围](https://www.commonwl.org/v1.0/CommandLineTool.html#Scope)

  [1.5术语](https://www.commonwl.org/v1.0/CommandLineTool.html#Terminology)

[2.数据模型](https://www.commonwl.org/v1.0/CommandLineTool.html#Data_model)

  [2.1数据概念](https://www.commonwl.org/v1.0/CommandLineTool.html#Data_concepts)

  [2.2语法](https://www.commonwl.org/v1.0/CommandLineTool.html#Syntax)

  [2.3标识符](https://www.commonwl.org/v1.0/CommandLineTool.html#Identifiers)

  [2.4 文件预处理](https://www.commonwl.org/v1.0/CommandLineTool.html#Document_preprocessing)

  [2.5扩展和元数据](https://www.commonwl.org/v1.0/CommandLineTool.html#Extensions_and_metadata)

[3.执行模型](https://www.commonwl.org/v1.0/CommandLineTool.html#Execution_model)

  [3.1 执行概念](https://www.commonwl.org/v1.0/CommandLineTool.html#Execution_concepts)

  [3.2通用执行过程](https://www.commonwl.org/v1.0/CommandLineTool.html#Generic_execution_process)

  [3.3 要求和提示](https://www.commonwl.org/v1.0/CommandLineTool.html#Requirements_and_hints)

  [3.4参数引用](https://www.commonwl.org/v1.0/CommandLineTool.html#Parameter_references)

  [3.5 表达式](https://www.commonwl.org/v1.0/CommandLineTool.html#Expressions)

  [3.6 以脚本形式执行CWL文档](https://www.commonwl.org/v1.0/CommandLineTool.html#Executing_CWL_documents_as_scripts)

  [3.7 在本地文件系统上发现CWL文档](https://www.commonwl.org/v1.0/CommandLineTool.html#Discovering_CWL_documents_on_a_local_filesystem)

[4.运行命令](https://www.commonwl.org/v1.0/CommandLineTool.html#Running_a_Command)

  [4.1 输入绑定](https://www.commonwl.org/v1.0/CommandLineTool.html#Input_binding)

  [4.2运行时环境](https://www.commonwl.org/v1.0/CommandLineTool.html#Runtime_environment)

  [4.3 执行](https://www.commonwl.org/v1.0/CommandLineTool.html#Execution)

  [4.4 输出绑定](https://www.commonwl.org/v1.0/CommandLineTool.html#Output_binding)

[5.CommandLineTool](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandLineTool)

  [5.1 CommandInputParameter](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputParameter)

  [5.1.1 表达](https://www.commonwl.org/v1.0/CommandLineTool.html#Expression)

  [5.1.2 CommandLineBinding](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandLineBinding)

  [5.1.3 任何](https://www.commonwl.org/v1.0/CommandLineTool.html#Any)

  [5.1.4 CWL类型](https://www.commonwl.org/v1.0/CommandLineTool.html#CWLType)

  [5.1.5 文件](https://www.commonwl.org/v1.0/CommandLineTool.html#File)

  [5.1.5.1目录](https://www.commonwl.org/v1.0/CommandLineTool.html#Directory)

  [5.1.6 CommandInputRecordSchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputRecordSchema)

  [5.1.7 CommandInputRecord字段](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputRecordField)

  [5.1.7.1 CommandInputEnumSchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputEnumSchema)

  [5.1.7.2 CommandInputArraySchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputArraySchema)

[5.2 CommandOutputParameter](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputParameter)

  [5.2.1 stdout](https://www.commonwl.org/v1.0/CommandLineTool.html#stdout)

  [5.2.2 stderr](https://www.commonwl.org/v1.0/CommandLineTool.html#stderr)

  [5.2.3 CommandOutputBinding](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputBinding)

  [5.2.4 CommandOutputRecordSchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputRecordSchema)

  [5.2.5 CommandOutputRecordField](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputRecordField)

  [5.2.5.1 CommandOutputEnumSchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputEnumSchema)

  [5.2.5.2 CommandOutputArraySchema](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandOutputArraySchema)

[5.3 内联Javascript要求](https://www.commonwl.org/v1.0/CommandLineTool.html#InlineJavascriptRequirement)

[5.4 架构要求](https://www.commonwl.org/v1.0/CommandLineTool.html#SchemaDefRequirement)

  [5.4.1 输入记录模式](https://www.commonwl.org/v1.0/CommandLineTool.html#InputRecordSchema)

  [5.4.2 输入记录字段](https://www.commonwl.org/v1.0/CommandLineTool.html#InputRecordField)

  [5.4.2.1 输入EnumSchema](https://www.commonwl.org/v1.0/CommandLineTool.html#InputEnumSchema)

  [5.4.2.2 输入数组模式](https://www.commonwl.org/v1.0/CommandLineTool.html#InputArraySchema)

[5.5 Docker要求](https://www.commonwl.org/v1.0/CommandLineTool.html#DockerRequirement)

[5.6软件要求](https://www.commonwl.org/v1.0/CommandLineTool.html#SoftwareRequirement)

[5.7 软件包](https://www.commonwl.org/v1.0/CommandLineTool.html#SoftwarePackage)

[5.8 InitialWorkDir要求](https://www.commonwl.org/v1.0/CommandLineTool.html#InitialWorkDirRequirement)

  [5.8.1 负责人](https://www.commonwl.org/v1.0/CommandLineTool.html#Dirent)

[5.9 环境要求](https://www.commonwl.org/v1.0/CommandLineTool.html#EnvVarRequirement)

[5.10](https://www.commonwl.org/v1.0/CommandLineTool.html#EnvironmentDef) [EnvironmentDef](https://www.commonwl.org/v1.0/CommandLineTool.html#EnvironmentDef)

[5.11 ShellCommand要求](https://www.commonwl.org/v1.0/CommandLineTool.html#ShellCommandRequirement)

[5.12 所需资源](https://www.commonwl.org/v1.0/CommandLineTool.html#ResourceRequirement)

[5.13 CWL版本](https://www.commonwl.org/v1.0/CommandLineTool.html#CWLVersion)

# 1.导言

通用工作流程语言（CWL）工作组是一个非正式的多开发者工作组，由对数据分析工作流程的可移植性感兴趣的各种组织和个人组成。目标是创建这样的规范，使数据科学家能够描述功能强大、易于使用、便携和支持可复制性的分析工具和工作流程。

## 1.1CWL命令行工具标准v1.0.2简介

本规范代表CWL组的第三个稳定版本。自v1.0初始发布以来，v1.0.2对CWL命令行工具标准进行了以下更新。文档应继续使用`cwlVersion: v1.0`，同时现有的v1.0文档仍然有效，但依赖之前未定义或未指定不足行为的CWL文档在v1.0.2中的行为可能略有不同。

- 2016年7月13日：将`baseCommand`标记为可选并更新描述性文本。

- 2016年11月14日：澄清[软件要求](https://www.commonwl.org/v1.0/CommandLineTool.html#SoftwareRequirement)`spec`字段。

- 2017年3月12日：

    - 将`default`标记为不需要链接检查。

    - 补充说明，InitialWorkDir中的文件必须有输出目录的路径。

    - 添加可写：true递归适用的注释。

- 2017年7月23日：（v1.0.1）

    - 添加有关分散在空数组上的申明。

    - 对输入`secondaryFiles`的解释。

    - 扩展对`File`和`Directory`类型语义的讨论

    - 修复了“EMACScript”到“ECMAScript”的错别字

    - 澄清了当输入为`null`或未定义时输入参数默认值的应用。

    - 澄清了输入与输出格式字段的有效类型和含义

    - 澄清命令行参数不得解释为shell，除非shellQuote：false

    - 澄清行为`entryname`

- 2017年8月10日：（v1.0.2）

    - 澄清在`secondaryFile`

自草案-3以来，v1.0对CWL命令行工具标准进行了以下更改和添加：

- [目录](https://www.commonwl.org/v1.0/CommandLineTool.html#Directory)类型。

- 语法简化：由`map<>`语法表示。示例：输入包含一个项目列表，每个项目都有一个ID。现在，您可以指定该标识符与相应的`CommandInputParamater`的映射。

```text
inputs:
 - id: one
   type: string
   doc: First input parameter
 - id: two
   type: int
   doc: Second input parameter
```

```text
inputs:
 one:
  type: string
  doc: First input parameter
 two:
  type: int
  doc: Second input parameter
```

- [InitialWorkDirRequirement](https://www.commonwl.org/v1.0/CommandLineTool.html#InitialWorkDirRequirement)：执行前将显示在输出目录中的文件和子目录列表。

- 用于指定标准输出和/或错误流为（可流式）文件输出的快捷方式。

- 用于描述工具软件依赖性的软件[要求](https://www.commonwl.org/v1.0/CommandLineTool.html#SoftwareRequirement)。

- 公共`description`字段已重命名为`doc`。

## 1.2目的

独立程序是一种灵活的、可互操作的代码重用形式。与单体程序不同，由多个独立程序组成的应用程序和分析工作流程可以用多种语言编写，并在多个主机上并发执行。然而，POSIX并没有为程序的输入和输出规定计算机可读的语法或语义，导致程序之间的命令行语法和输入/输出语义极其不一致。这在分布式计算（多节点计算集群）和虚拟化环境（如Docker容器）中是一个特别的问题，在这些环境中往往需要在执行程序之前提供资源，如输入文件。

通常，通过硬编码程序调用并隐含地假设满足要求，或使用包装脚本或描述符文档抽象程序调用来填补这一空白。不幸的是，当这些方法特定于应用程序或平台时，它会对可复制性和可移植性造成重大障碍，因为为一个平台开发的方法必须手动移植才能在新平台上使用。同样，它创造了冗余的工作，因为流行工具的包装必须为使用中的每个应用程序或平台重写。

通用工作流程语言命令行工具描述旨在为调用生物信息学、化学、物理、天文学和统计学等数据密集型领域的程序提供通用语法和语义描述。本规范定义了命令行工具的精确数据和执行模型，可以在从单个工作站到集群、网格、云和高性能计算平台的各种计算平台上实现。

## 1.3 提及其他规格

**Javascript对象符号（JSON）：**[http://json.org](http://json.org/)

**JSON链接数据（JSON-LD）：**[http://json-ld.org](http://json-ld.org/)

**YAML**：[http://yaml.org](http://yaml.org/)

**Avro**：[https://avro.apache.org/docs/1.8.1/spec.html](https://avro.apache.org/docs/1.8.1/spec.html)

**统一资源标识符（URI）通用语法**：[https://tools.ietf.org/html/rfc3986](https://tools.ietf.org/html/rfc3986)）

**国际化资源标识符（IRI）：**[https://tools.ietf.org/html/rfc3987](https://tools.ietf.org/html/rfc3987)

**便携式操作系统接口（POSIX.1-2008）：**[http://pubs.opengroup.org/onlinepubs/9699919799/](http://pubs.opengroup.org/onlinepubs/9699919799/)

**资源描述框架（RDF）：**[http://www.w3.org/RDF/](http://www.w3.org/RDF/)

1.4范围

本文介绍了CWL语法、执行和对象模型。它不打算记录CWL特定的实现，但它可以作为符合实现行为的参考。

## 1.5术语

用于描述CWL文档的术语在规范的概念部分中定义。以下列表中定义的术语用于构建这些定义和描述CWL实施的行动：

**可能**：允许符合CWL文档和CWL实现，但不需要按照描述行事。

**必须**：符合CWL文档和CWL实现必须按描述行事；否则它们是错误的。

**错误**：违反本规范的规则；结果未定义。符合的实现可能会检测和报告错误，并可能从中恢复。

**致命错误**：违反本规范的规则；结果未定义。符合要求的实现不得继续执行当前进程，并可能报告错误。

**在用户选项**：合规软件可以或必须（取决于句子中的情态动词）按照描述的方式行事；如果这样做了，它必须为用户提供启用或禁用所述行为的手段。

**不建议使用**：符合要求的软件可能会实现向后兼容性的行为。便携式CWL文档不应依赖于不建议使用的行为。标记为不建议使用的行为可能会从CWL规范的未来修订中完全删除。

# 2.数据模型

## 2.1数据概念

**对象**是相当于JSON中“对象”类型的数据结构，由一组无序的名称/值对（此处称为**字段**）组成，名称为字符串，值为字符串、数字、布尔值、数组或对象。

**文档**是包含序列化对象或对象数组的文件。

**过程**是接受输入数据、执行一些计算并产生输出数据的基本计算单位。示例包括CommandLineTools、工作流程和ExpressionTools。

**输入对象**是描述进程调用的输入的对象。

**输出对象**是描述调用进程所产生的输出的对象。

**输入模式**描述了输入对象的有效格式（必填字段、数据类型）。

**输出模式**描述了输出对象的有效格式。

**元数据**是关于工作流程、工具或输入项的信息。

### 2.2语法

CWL文档必须由使用JSON或YAML语法表示的对象或对象数组组成。加载时，CWL实现必须应用[链接Avro数据（SALAD）规范的语义注释](https://www.commonwl.org/v1.0/SchemaSalad.html)中描述的预处理步骤。实现可以使用位于[https://github.com/common-workflow-language/common-workflow-language/tree/master/v1.0](https://github.com/common-workflow-language/common-workflow-language/tree/master/v1.0)的SALAD模式正式验证CWL文档的结构。

### 2.3标识符

如果一个对象包含一个id字段，它被用来唯一地识别该文件中的对象。id字段的值在整个文档中必须是唯一的。标识符可以按照[Schema Salad规范](https://www.commonwl.org/v1.0/SchemaSalad.html#Identifier_resolution)中描述的规则，相对于文档基础和/或其他标识符进行解析。

一个实现可以选择只对本规范中明确列出的id字段的对象类型的引用。

### 2.4文档预处理

实现必须按照[Schema Salad规范](https://www.commonwl.org/v1.0/SchemaSalad.html#Identifier_resolution)中所述解决[$import](https://www.commonwl.org/v1.0/SchemaSalad.html#Import)和[$include](https://www.commonwl.org/v1.0/SchemaSalad.html#Import)指令。

Schema salad中定义的另一个转换是简化数据类型定义。键入以`?`结尾的`<T>`应该转换为`[<T>, "null"]`。类型`<T>`以`[]`应该改成`{"type": "array", "items": <T>}`

### 2.5扩展和元数据

输入元数据（例如，实验室样品标识符）可以在一个工具或工作流程中使用输入参数来表示，这些参数会明确地传播到输出。本规范的未来版本可能会定义额外的工具用于处理输入/输出元数据。

正确执行不需要的实现扩展（例如，与GUI演示相关的字段）和有关工具或工作流程本身的元数据（例如，用于引用的作者）可以作为任何对象上的其他字段提供。如[Schema Salad规范](https://www.commonwl.org/v1.0/SchemaSalad.html#Explicit_context)所述，此类扩展字段必须使用文档的`$namespaces`部分中列出的命名空间前缀。修改执行语义的实现扩展必须在`requirements`字段中列出。

# 3.执行模型

## 3.1 执行概念

**参数**是进程的命名符号输入或输出，具有关联的数据类型或模式。在执行过程中，为参数分配值，以使输入对象或输出对象用于具体过程调用。

**CommandLineTool**是一个过程，其特点是执行一个独立的非交互式程序，该程序在某些输入上调用，产生输出，然后终止。

**工作流**是一个以多个子进程步骤为特征的过程，其中步骤输出连接到下游步骤的输入，以形成一个定向无环图，独立的步骤可以同时运行。

**运行时环境**是指执行命令行工具时的实际硬件和软件环境。它包括但不限于运行工具所需的硬件结构、硬件资源、操作系统、软件运行时间（如果适用的话，例如特定的Python解释器或特定的Java虚拟机）、库、模块、包、实用程序和数据文件。

工作流**平台**是一种特定的硬件和软件实现，能够解释CWL文档并执行文档指定的流程。工作流平台的职责可能包括安排流程调用、设置必要的运行时环境、提供输入数据、调用工具流程和收集输出。

工作流平台可以选择仅实现CWL规范中的命令行工具描述部分。

我们希望工作流平台在本规范之外有广泛的余地，以优化计算资源的使用，并执行本规范未涵盖的政策。目前不属于CWL规范但可能由特定工作流程平台处理的一些领域包括：

- 数据安全和权限

- 在远程集群或云计算节点上安排工具调用。

- 使用虚拟机或操作系统容器来管理运行时（[DockerRequirement](https://www.commonwl.org/v1.0/CommandLineTool.html#DockerRequirement)中所述除外）。

- 使用远程或分布式文件系统管理输入和输出文件。

- 转换文件路径。

- 确定进程是否之前已执行，如果是，请跳过它并重用之前的结果。

- 暂停、恢复或检查流程或工作流程。

除非使用[流程要求](https://www.commonwl.org/v1.0/CommandLineTool.html#Requirements_and_hints)明确声明，否则符合CWL进程不得假设运行时环境或工作流平台。

### 3.2通用执行过程

CWL进程（包括工作流程和命令行工具）的通用执行顺序如下。

1. 加载、处理和验证CWL文档，生成进程对象。

1. 加载输入对象。

1. 针对进程的输入模式验证`inputs`对象。

1. 符合验证流程要求。

1. 执行特定流程类型所需的任何进一步设置。

1. 执行过程。

1. 将进程执行的结果捕获到输出对象中。

1. 针对进程的输出模式验证`outputs`对象。

1. 向进程调用方报告输出对象。

### 3.3要求和提示

**进程要求**修改进程的语义或运行时环境。如果实现无法满足所有要求，或者列出了一个实现无法识别的要求，这是一个致命的错误，除非在用户选项中重写，否则实现不得尝试运行该进程。

**提示**类似于要求；但是，如果实现无法满足所有提示，则这不是错误。如果提示无法满足，实现可能会报告警告。

**要求**是继承的。工作流程中指定的要求适用于所有工作流程步骤；工作流程步骤上指定的要求将适用于该步骤及其任何子步骤的流程实现。

如果相同的流程要求出现在工作流程的不同级别，则使用该需求的最具体实例，即命令行工具等流程实现中`requirements`条目将优先于工作流步骤中指定`requirements`条目，而工作流步骤上`requirements`条目优先于工作流程。`hints`中的条目以同样的方式解决。需求覆盖提示。如果流程实现以`hints`的形式提供了流程要求，而提示中也通过封闭的工作流或工作流步骤在`requirements`中提供，则封闭`requirements`优先。

## 3.4参数引用

参数引用由语法`$(...)`表示，并可用于本文档指定的允许伪类型`Expression`的任何字段。符合条件的实现必须支持参数引用。参数引用使用以下[Javascript/ECMAScript 5.1](http://www.ecma-international.org/ecma-262/5.1/)语法子集，但它们的设计不需要Javascript引擎进行评估。

在以下BNF语法中，字符类和语法规则以“{}”表示，'-'表示对字符类的排除，'(())'表示分组，'|'表示交替，尾随的'*'表示零次或更多重复，'+'表示一个或多个重复，'/'转义这些特殊字符，所有其他字符均为字面值。

| symbol::              | {Unicode alphanumeric}+                      |
| --------------------- | -------------------------------------------- |
| singleq::             | [' (( {character - '} | \' ))* ']            |
| doubleq::             | [" (( {character - "} | \" ))* "]            |
| index::               | [ {decimal digit}+ ]                         |
| segment::             | . {symbol} | {singleq} | {doubleq} | {index} |
| parameter reference:: | $( {symbol} {segment}*)                      |

使用以下算法解决参数引用：

1. 将前导符号匹配为键

1. 在参数上下文中查找该键（如下所述）以获得当前值。如果在参数上下文中没有找到该键，则是一个错误。

1. 如果没有后续段，请终止并返回当前值

1. 否则，匹配下一部分

1. 从段中提取符号、字符串或索引作为键

1. 在当前值中查找键并作为新的当前值分配。如果键是一个符号或字符串，当前值必须是一个对象。如果键是一个索引，当前值必须是一个数组或字符串。如果键不符合要求的类型，或者没有找到键或超出范围，则是一个错误。

1. 重复步骤3-6

根命名空间是参数上下文。必须提供以下参数：

- `inputs`：当前进程的输入对象。

- `self`：特定于上下文的值。本规范其他地方的特定字段记录了“self”的上下文的值。如果没有记录字段的“self”上下文的值，则必须是“null”。

- `runtime`：包含配置详细信息的对象。特定于流程类型。实现可以为`runtime`的任何或所有字段提供不透明的字符串。这些必须在处理工具后但在实际执行之前由平台填写。参数引用和表达式只能使用字段的字面字符串值，除非另有说明，否则不得对内容执行计算。

如果字段的值在参数引用周围没有前导或尾随非空格字符，则字段的有效值将成为引用参数的值，从而保留返回类型。

如果字段的值在参数引用周围有非空格前导字符或后导字符，则需要字符串插值。该字段的有效值是一个包含前导字符的字符串，后跟参数引用的字符串值，后跟尾随字符。参数引用的字符串值是其文本JSON表示形式，具有以下规则：

- 前导引号和后引号从字符串中删除

- 对象条目按键排序

多个参数引用可能会出现在一个字段中。此情况必须视为字符串插值。插值第一个参数引用后，必须递归地将插值应用于后跟字符，才能生成最终字符串值。

### 3.5 表达式

表达式是工作流程平台评估的[Javascript/ECMAScript 5.1](http://www.ecma-international.org/ecma-262/5.1/)代码的片段，以影响进程的输入、输出或行为。在通用执行序列中，表达式可以在第5步（流程设置）、第6步（执行流程）和/或第7步（捕获输出）期间进行评估。表达式与常规进程的不同之处在于，它们旨在修改工作流本身的行为，而不是执行工作流的主要工作。

要声明表达式的使用，文档必须包含进程要求`InlineJavascriptRequirement`。表达式可用于本文档指定的允许伪类型`Expression`的任何字段。表达式由语法`$(...)`或`${...}`表示。用`$(...)`语法包装的代码片段必须计算为[ECMAScript表达式](http://www.ecma-international.org/ecma-262/5.1/#sec-11)。包裹在`${...}`语法中的代码片段必须评估为匿名零参数函数的[ECMAScript函数体](http://www.ecma-international.org/ecma-262/5.1/#sec-13)。表达式必须返回有效的JSON数据类型：空、字符串、数字、布尔、数组、对象之一。其他返回值必须导致`permanentFailure`。实现必须允许任何语法有效的Javascript，并说明括号或大括号的嵌套，以及扫描表达式时可能包含括号或大括号的字符串。

在执行实际表达式之前，运行时必须包括[InlineJavascriptRequirement的“expressionLib”字段](https://www.commonwl.org/v1.0/CommandLineTool.html#InlineJavascriptRequirement)中定义的任何代码。

在执行表达式之前，运行时必须将上述参数上下文的字段初始化为全局变量。

表达式评估后字段的有效值遵循与上述参数引用相同的规则。多个表达式可能会出现在单个字段中。

表达式必须在孤立的上下文（“sandbox”）中进行评估，该上下文不允许任何副作用泄漏到上下文之外。表达式还必须在[Javascript严格模式下](http://www.ecma-international.org/ecma-262/5.1/#sec-4.2.2)计算。

除非本文档中另有说明，否则表达式的计算顺序未定义。

实现可以选择通过评估为Javascript表达式来实现参数引用。无论通过Javascript评估还是其他方式实现，评估参数引用的结果都必须相同。

实现可能会应用其他限制，如进程隔离、超时和操作系统容器/jail，以尽量减少与运行CWL文档中嵌入的不受信任代码相关的安全风险。

从表达式中抛出的异常必须导致进程`permanentFailure`。

### 3.6将CWL文档作为脚本执行

按照惯例，CWL文件可以以`#!/usr/bin/env cwl-runner`开头，并被标记为可执行（POSIX "+x "权限位），以使其能够直接执行。工作流平台可能支持这种操作模式；如果是这样，它必须提供`cwl-runner`作为平台的CWL实现的别名。CWL输入对象文件同样可以以`#！/usr/bin/env cwl-runner`开始，并被标记为可执行。在这种情况下，输入对象必须包括`cwl:tool`字段，提供默认CWL文件的IRI，该文件应使用输入对象的字段作为输入参数执行。

### 3.7 在本地文件系统上发现CWL文档

要发现CWL文档，请查看以下位置：

`/usr/share/commonwl//usr/local/share/commonwl/$XDG_DATA_HOME/commonwl/`（通常为`$HOME/.local/share/commonwl`）`$XDG_DATA_HOME`来自[XDG基本目录规范](http://standards.freedesktop.org/basedir-spec/basedir-spec-0.6.html)

### 4.运行命令

为了适应任意程序的输入、运行时环境、调用和输出的语法和语义的巨大多样性，CommandLineTool定义了一个“输入绑定”，描述了如何将抽象输入参数转换为具体的程序调用，以及一个“输出绑定”，描述了如何从程序输出生成输出参数。

### 4.1 输入绑定

工具命令行是通过将命令行绑定应用于输入对象来构建的。绑定要么使用[输入](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputParameter)`inputBinding`字段作为[输入参数](https://www.commonwl.org/v1.0/CommandLineTool.html#CommandInputParameter)的一部分列出，要么使用CommandLineTool的`arguments`字段单独列出。

构建命令行的算法如下。在此算法中，排序键是由一个或多个数字或字符串元素组成的列表。字符串基于UTF-8编码按词典排序。

1. 从`arguments`中收集`CommandLineBinding`对象。指定一个排序键`[position, i]`，其中`position`是`CommandLineBinding.position`，`i`是`arguments`列表中的索引。

2. 从`inputs`模式中收集`CommandLineBinding`对象，并将其与输入对象的值关联。如果输入类型是记录、数组或映射，则递归走模式和输入对象，收集嵌套的`CommandLineBinding`对象，并将其与输入对象的值关联。

3.通过获取通往每个子绑定对象的每一级的`position`字段的值来创建一个排序键。如果没有指定`position`，它不会被添加到排序键中。对于数组和maps的绑定，排序键必须包括位置之后的数组索引或maps键。如果且仅当两个绑定具有相同的排序键时，必须使用紧紧包含子绑定的字段或参数名称的排序来打破这种联系。

4. 使用分配的排序键对元素进行排序。数字条目在字符串之前排序。

5. 按照排序顺序，应用`CommandLineBinding`中定义的规则将绑定转换为实际命令行元素。

6. 在命令行开头插入`baseCommand`中的元素。

### 4.2运行时环境

输入对象中列出的所有文件都必须在运行时环境中可用。实现可以使用共享或分布式文件系统，或通过显式下载将文件传输到主机。实现可以选择不提供对输入对象或进程要求中未明确指定的文件的访问。

工具执行生成的输出文件必须写入**指定的输出目录**。执行工具时，初始当前工作目录必须是指定的输出目录。

文件也可以写入**指定的临时目录**。此目录必须隔离，而不是与其他进程共享。写入指定临时目录的任何文件都可以在工具终止后立即由工作流程平台自动删除。

为了兼容性，文件可以写入**系统临时目录**，该**目录**必须位于`/tmp`。由于系统临时目录可能与系统上的其他进程共享，因此放置在系统临时目录中的文件不能保证自动删除。工具不得将系统临时目录用作与其他工具的后台通信。系统临时目录与指定的临时目录相同是有效的。

执行工具时，该工具必须在新的空环境中执行，仅使用以下描述的环境变量；子进程不得从父进程中继承环境变量，除非指定或按用户选项。

- `HOME`必须设置为指定的输出目录。

- `TMPDIR`必须设置为指定的临时目录。

- `PATH`可以从父进程继承，除非在提供自己`PATH`的容器中运行。

- 由[EnvVar要求](https://www.commonwl.org/v1.0/CommandLineTool.html#EnvVarRequirement)定义的变量

- 容器的默认环境，例如使用[DockerRequirement](https://www.commonwl.org/v1.0/CommandLineTool.html#DockerRequirement)时

实现可以禁止该工具写入运行时环境文件系统中除指定临时目录、系统临时目录和指定输出目录以外的任何位置。实现可能会提供只读输入文件，并不允许就地更新输入文件。指定的临时目录、系统临时目录和指定的输出目录可能都驻留在不同文件系统的不同挂载点上。

实现可能会禁止该工具直接访问网络资源。正确的工具不得假设任何网络访问。规范的未来版本可能会包含描述工具网络需求的可选流程要求。

[参数引用](https://www.commonwl.org/v1.0/CommandLineTool.html#Parameter_references)和[表达式](https://www.commonwl.org/v1.0/CommandLineTool.html#Expressions)中可用的`runtime`部分包含以下字段。如前所述，实现可以通过为以下任何或所有字段提供不透明的字符串来执行运行时字段的延迟解析；参数引用和表达式只能使用字段的字面字符串值，不得对内容执行计算。

- `runtime.outdir`：指定输出目录的绝对路径

- `runtime.tmpdir`：指向指定临时目录的绝对路径

- `runtime.cores`：为工具流程保留的CPU内核数量

- `runtime.ram`：为工具流程保留的RAM量（以兆字节为单位（2**20））

- `runtime.outdirSize`：指定输出目录中可用的保留存储空间

- `runtime.tmpdirSize`：指定临时目录中可用的预留存储空间

对于`cores`、`ram`、 `outdirSize`和`tmpdirSize`，如果实现在表达式评估期间无法提供保留核心的实际数量，它应该报告最低请求量。

有关如何描述工具所需的硬件资源的详细信息，请参阅[资源要求](https://www.commonwl.org/v1.0/CommandLineTool.html#ResourceRequirement)。

标准输入流和标准输出流可以按照`stdin`和`stdout`字段所述重定向。

### 4.3 执行

构建命令行并创建运行时环境后，将执行实际工具。

平台日志设施可以捕获标准错误流和标准输出流（除非通过设置`stdout`或`stderr`重定向），以进行存储和报告。

工具可以是多线程或生成子进程；但是，当父进程退出时，无论是否有任何分离的子进程仍在运行，该工具都被视为已完成。工具不得要求任何类型的控制台、GUI或基于Web的用户交互才能开始并运行到完成。

进程的退出代码指示进程是否成功完成。根据惯例，零退出代码被视为成功，非零退出代码被视为失败。这可以通过提供字段`successCodes`、`temporaryFailCodes`和`permanentFailCodes`定义。实现可以选择将未指定的非零退出代码默认为`temporaryFailure`或`permanentFailure`。

### 4.4 输出绑定

如果输出目录包含一个名为 "cwl.output.json "的文件，该文件必须被加载并作为输出对象使用。否则，输出对象必须通过行走`outputs`中列出的参数和应用输出绑定到工具输出来生成。输出绑定是使用`outputBinding`字段与输出参数相关联的。详见`CommandOutputBinding`。

## 5.CommandLineTool 

这定义了CWL命令行工具描述文档的模式。

### 字段

| 字段                 | 必填             | 类型                                                                                                                                                                                                                                                                                                                                                                                           | 描述                                                                                                                                                                                                                                                                                                                                                                    |
| ------------------ | -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| inputs             | requiredarray  | \<CommandInputParameter> \ map<id，type|CommandInputParameter>                                                                                                                                                                                                                                                                                                                                                                       | 定义过程的输入参数。当所有必需的输入参数与具体值相关联时，该过程就可以运行了。输入参数包括每个参数的模式，用于验证输入对象。它还可用于构建用于构建输入对象的用户界面。接受输入对象时，所有输入参数都必须有一个值。如果输入对象中缺少输入参数，则必须为其分配一个null值（如果提供该参数的default值），以便验证和评估表达式。                                                                                                                                                                                                                                  |
| outputs            | requiredarray  | \<CommandOutputParameter> \ map<id，type|CommandOutputParameter>                                                                                                                                                                                                                                                                                                                                | 定义表示进程输出的参数。可用于生成和/或验证输出对象。                                                                                                                                                                                                                                                                                                                                           |
| class              | 必填             | 字符串                                                                                                                                                                                                                                                                                                                                                                                          |                                                                                                                                                                                                                                                                                                                                                                       |
| id                 | 可选             | 字符串                                                                                                                                                                                                                                                                                                                                                                                          | 此进程对象的唯一标识符。                                                                                                                                                                                                                                                                                                                                                          |
| requirements       | optionalarray  | <InlineJavascriptRequirement |SchemaDefRequirement |DockerRequirement |SoftwareRequirement |InitialWorkDirRequirement |EnvVarRequirement |ShellCommandRequirement |ResourceRequirement> |map<class, InlineJavascriptRequirement | SchemaDefRequirement |DockerRequirement |SoftwareRequirement |InitialWorkDirRequirement |EnvVarRequirement | ShellCommandRequirement |ResourceRequirement> | 声明适用于运行时环境或工作流程引擎的要求，这些要求必须满足才能执行此过程。如果实现无法满足所有要求，或者列出了一个实现无法识别的要求，这是一个致命的错误，除非在用户选项中重写，否则实现不得尝试运行该进程。                                                                                                                                                                                                                                                                |
| hints              | optionalarray  | \<Any> \ map<class, Any>                                                                                                                                                                                                                                                                                                                                                                      | 声明应用于运行时环境或工作流引擎的提示，这些提示可能有助于执行此过程。如果实现不能满足所有提示，则这不是错误，但实现可能会报告警告。                                                                                                                                                                                                                                                                                                    |
| label              | 可选             | 字符串                                                                                                                                                                                                                                                                                                                                                                                          | 此过程对象的简短、可读的标签。                                                                                                                                                                                                                                                                                                                                                       |
| doc                | 可选             | 字符串                                                                                                                                                                                                                                                                                                                                                                                          | 对这个过程对象的长而可读的描述。                                                                                                                                                                                                                                                                                                                                                      |
| cwlVersion         | 可选             | CWLVersion                                                                                                                                                                                                                                                                                                                                                                                   | CWL文档版本。始终需要在文档根目录处完成。嵌入到另一个进程中的进程不需要。                                                                                                                                                                                                                                                                                                                                |
| baseCommand        | optionalstring | array\<string>                                                                                                                                                                                                                                                                                                                                                                              | 指定要执行的程序。如果是数组，数组的第一个元素是要执行的命令，后续元素是强制性命令行参数。baseCommand中的元素必须出现在inputBinding或arguments的任何命令行绑定之前。如果没有提供baseCommand或者是一个空数组，那么在处理完inputBinding或者arguments后产生的命令行的第一个元素必须被用作要执行的程序。如果程序包含路径分隔符字符，则必须是绝对路径，否则是一个错误。如果程序不包含路径分隔符，请在工作流运行时环境中搜索$PATH变量，找到可执行文件的绝对路径。 |
| arguments          | optionalarray  | \<string> | 表达式 | CommandLineBinding>                                                                                                                                                                                                                                                                                                                                                          | 与输入参数没有直接关联的命令行绑定                                                                                                                                                                                                                                                                                                                                                     |
| stdin              | optionalstring | | 表达式                                                                                                                                                                                                                                                                                                                                                                                        | 文件的路径，其内容必须管道传输到命令的标准输入流中。                                                                                                                                                                                                                                                                                                                                            |
| stderr             | optionalstring | | 表达式                                                                                                                                                                                                                                                                                                                                                                                        | 将命令的标准错误流捕获到写入指定输出目录的文件。如果stderr是一个字符串，它指定要使用的文件名。如果stderr是一个表达式，则该表达式将进行计算，并且必须返回一个带有文件名的字符串才能用于捕获stderr。如果返回值不是字符串，或者生成的路径包含非法字符（如路径分隔符/），则为错误。                                                                                                                                                                                                                     |
| stdout             | optionalstring | | 表达式                                                                                                                                                                                                                                                                                                                                                                                        | 将命令的标准输出流捕获到写入指定输出目录的文件。如果stdout是一个字符串，则指定要使用的文件名。如果stdout是一个表达式，则对该表达式进行计算，并且必须返回一个带有文件名的字符串才能用于捕获stdout。如果返回值不是字符串，或者生成的路径包含非法字符（如路径分隔符/），则为错误。                                                                                                                                                                                                                     |
| successCodes       | 可选             | 数组\<int>                                                                                                                                                                                                                                                                                                                                                                                      | 指示成功完成流程的退出代码。                                                                                                                                                                                                                                                                                                                                                        |
| temporaryFailCodes | 可选             | 数组\<int>                                                                                                                                                                                                                                                                                                                                                                                      | 退出代码，指示进程因可能暂时条件而失败，在相同运行时环境和输入的情况下执行进程可能会产生不同的结果。                                                                                                                                                                                                                                                                                                                    |
| permanentFailCodes | 可选             | 数组\<int>                                                                                                                                                                                                                                                                                                                                                                                      | 退出代码，指示进程因永久逻辑错误而失败，其中使用相同的运行时环境和相同的输入执行进程预计将始终失败。                                                                                                                                                                                                                                                                                                                    |

### 5.1 CommandInputParameter 

CommandLineTool的输入参数。

### 字段
<style>
table th:first-of-type {
    width: 10%;
}
table th:nth-of-type(2) {
    width: 20%;
}
table th:nth-of-type(3) {
    width: 20%;
}
table th:nth-of-type(4) {
    width: 50%;
}
</style>


| 字段             | 必填                         | 类型                                                                                                                                                                                      | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------------------ | -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id             | 必填                         | 字符串                                                                                                                                                                                                                                                                                                                                                                                              | 此参数对象的唯一标识符。                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| label          | 可选                         | 字符串                                                                                                                                                                                     | 这个物体的简短、可读的标签。                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| secondaryFiles | optionalstring             | | 表达式 | array\<string | Expression>                                                                                                                                                      | 只有当类型是文件或items:File列是有效。提供模式或表达式，指定必须与主文件一起包含的文件或目录。所有列出的辅助文件都必须存在。如果不存在预期的辅助文件，则实现可能无法执行工作流程。如果该值是表达式，则表达式中的self值必须是此绑定适用的主输入或输出文件对象。basename、nameroot和nameext字段必须以self存在。ForCommandLineTool输出path字段也必须存在。表达式必须返回相对于主文件路径的文件名字符串、设置path或location和basename字段的文件或目录对象，或由字符串或文件或目录对象组成的数组。将从输入中提取的未更改的文件或目录对象引用为辅助文件是合法的。要处理非文件名保存存储系统，便携式工具描述应避免从location构造新值，而应改用basename或nameroot构建相对引用。如果secondaryFiles中的值不是表达式的字符串，则它指定应将以下模式应用于主文件的路径，以生成相对于主文件的文件名： |
| streamable     | 可选                         | 布尔值                                                                                                                                                                                     | Only valid when type: File or is an array of items: File.true值表示文件是按顺序读取或写入的，而不寻求。实现可以使用此标志来指示使用命名管道流化文件内容是否有效。默认：false。                                                                                                                                                                                                                                                                                                                                                                  |
| doc            | optionalstring             | | array\<string>                                                                                                                                                                         | 此类型的文档字符串，或应串联的字符串数组。                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| format         | optionalstring             | | array\<string> | 表达式                                                                                                                                                                   | Only valid when type: File or is an array of items: File.这必须是一个或多个概念节点的IRI，代表文件格式，允许作为此参数的输入，最好在本体中定义。如果没有可用的本体，文件格式可以通过完全匹配进行测试。                                                                                                                                                                                                                                                                                                                                                         |
| inputBinding   | optionalCommandLineBinding |                                                                                                                                                                                         | 描述如何处理进程的输入，并将其转换为具体的执行形式，如命令行参数。                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| type           | optionalCWLType            | |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema | string | array<CWLType |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema |string> | 指定可能分配给此参数的有效数据类型。                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |

### 5.1.1 表达式

“Expression”不是真正的类型。它指示字段必须允许运行时参数引用。如果平台声明并支持[InlineJavascriptRequirement](https://www.commonwl.org/v1.0/CommandLineTool.html#InlineJavascriptRequirement)，该字段还必须允许Javascript表达式。

### **符号**

| 符号                    | 描述  |
| --------------------- | --- |
| ExpressionPlaceholder |     |

### 5.1.2 CommandLineBinding 

当在输入模式的`inputBinding`下列出时，“值”一词是指输入对象中的相应值。对于`CommandLineTool.arguments`中列出的绑定对象，“值”一词是指评估`valueFrom`后的有效值。

构建命令行时的绑定行为取决于值的数据类型。如果输入模式描述的类型与有效值（例如表达式评估的结果）不匹配，实现必须使用有效值的数据类型。

- **字符串**：将`prefix`和字符串添加到命令行中。

- **数字**：向命令行添加`prefix`和小数表示形式。

- **布尔值**：如果为真，请在命令行中添加`prefix`。如果是假的，不要添加任何东西。

- **文件**：将`prefix`和`File.path`的值添加到命令行中。

- **Directory**：在命令行中添加`prefix`和`Directory.path`的值。

- **数组**：如果指定了`itemSeparator`，请将`prefix`和加入数组添加到单个字符串中，`itemSeparator`将项目分隔。否则，首先添加`prefix`，然后递归处理单个元素。如果数组为空，它不会向命令行添加任何内容。

- **对象**：仅添加`prefix`，并递归添加指定`inputBinding`的对象字段。

- **null**：不添加任何内容。

**字段**

| 字段            | 必填             | 类型          | 描述                                                                                                                                                                                                                                      |
| ------------- | -------------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| loadContents  | 可选             | 布尔值         | 只有当类型是文件或items:File列时有效。从文件中最多读取前64 KiB文本，并将其放在文件对象的“内容”字段中，供表达式使用。                                                                                                                                    |
| position      | 可选             | 排序键。默认位置为0。 |                                                                                                                                                                                                                                         |
| prefix        | 可选             | 字符串         | 在值之前添加命令行前缀。                                                                                                                                                                                                                            |
| separate      | 可选             | 布尔值         | 如果为true（默认），则前缀和值必须添加为单独的命令行参数；如果为false，前缀和值必须串联到单个命令行参数中。                                                                                                                                                                              |
| itemSeparator | 可选             | 字符串         | 将数组元素加入到单个字符串中，元素由byitemSeparator分隔                                                                                                                                                                                                     |
| valueFrom     | optionalstring | | 表达式       | 如果valueFrom是一个常量字符串值，请将其用作值并应用上面的绑定规则。如果valueFrom是一个表达式，则计算表达式以生成用于构建命令行的实际值，并应用上述绑定规则。如果inputBinding与输入参数相关联，表达式中的self值将是输入参数的值。在评估表达式之前，必须应用输入参数默认值（由InputParameter.default字段指定）。当绑定是CommandLineTool.arguments字段的一部分时，则需要valueFrom字段。 |
| shellQuote    | 可选             | 布尔值         | 如果ShellCommandRequirement在当前命令的要求中，则该命令控制该值是否在命令行上引用（默认为真）。使用shellQuote: false为管道等操作注入元字符。如果shellQuote为真或未提供，则实现不得允许解释任何shell元字符或指令。                                                                                                    |

### 5.1.3 任何

**Any**类型验证任何非空值。

**符号**

| 符号  | 描述  |
| --- | --- |
| Any |     |

### 5.1.4 CWLType 

以文件和目录为内置类型的概念扩展原始类型。

### 符号

| 符号        | 描述                  |
| --------- | ------------------- |
| null      | 没有价值                |
| boolean   | 二进制值                |
| int       | 32位有符号整数            |
| long      | 64位有符号整数            |
| float     | 单精度（32位）IEEE 754浮点数 |
| double    | 双精度（64位）IEEE 754浮点数 |
| string    | Unicode字符序列         |
| File      | 文件对象                |
| Directory | 目录对象                |

### 5.1.5 文件

表示使用标准POSIX文件系统调用API（如open（2）和read（2））的工具可以访问的文件（或提供`secondaryFiles`组文件）。文件表示为具有`Fileclass`的对象。文件对象有许多属性，可以提供有关文件的元数据。文件`location`属性是一个唯一标识文件的URI。实现必须支持file://URI计划，并可能支持http://等其他计划。`location`值也可以是相对引用，在这种情况下，它必须相对于它出现在文档的URI解析。或者，实现还必须接受文件上的`path`属性，该属性必须是与CWL运行器（用于输入）或命令行工具执行的运行时环境（用于命令行工具输出）在同一主机上可用的文件系统路径。如果没有指定`location`或`path`，文件对象必须指定带有文件UTF-8文本内容`contents`。这是一个“文件字面意思”。文件文本与外部资源不对应，而是在执行工具需要时在磁盘上创建`contents`。在适当的情况下，表达式可以返回文件文本，以定义运行时上的新文件。`contents`的最大大小为64千字节。`basename`属性定义了文件阶段磁盘上的文件名。这可能与资源名称不同。如果没有提供，`basename`必须从`location`的最后一个路径部分计算，并提供给表达式。`secondaryFiles`属性是文件或目录对象的列表，必须与主文件在同一目录中阶段。在`secondaryFiles`文件中复制文件名是一个错误。`size`属性是文件的字节大小。它必须从资源中计算，并提供给表达式。`checksum`字段包含文件内容的加密散列，用于验证文件内容。实现可以在用户选项下出于性能或其他原因启用或禁用`checksum`字段的计算。然而，通过CWL一致性测试套件需要计算输出校验和的能力。当执行CommandLineTool时，文件和二级文件可以被分档到一个任意的目录，但必须使用basename的值作为文件名。路径属性必须是工具执行运行时上下文中的文件路径（计算节点的本地，或在执行的容器内）。所有计算的属性都应该可以用于表达式。文件字面意义也必须被分期，并且路径必须被设置。收集CommandLineTool输出时，`glob`匹配将返回文件路径（带有`path`属性）和派生属性。这些都可以通过`outputEval`修改。或者，如果输出中存在`cwl.output.json`文件，则忽略`outputBinding`。输出中的文件对象必须在工具执行运行时的上下文中（计算机节点的本地或执行容器内）提供`location`URI或`path`属性。在评估ExpressionTool时，文件对象必须通过`location`（表达式工具无法访问磁盘上的文件，因此`path`毫无意义）或作为文件文本引用。返回具有现有`location`但不同`basename`的文件对象是合法的。ExpressionTool输入的`loadContents`字段的行为与CommandLineTool输入相同，但它对输出没有意义。ExpressionTool可以通过使用相同的`location`值将文件引用从输入转发到输出。

**字段**

| 字段             | 必填            | 类型      | 描述                                                                                                                                                                                                                                                                                                                                                         |
| -------------- | ------------- | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| class          | 必需            | 恒定值File | 必须为File才能指示此对象描述文件。                                                                                                                                                                                                                                                                                                                                        |
| location       | 可选            | 字符串     | 标识文件资源的IRI。这可能是一个相对的引用，在这种情况下，必须使用文档的基本IRI来解决。该位置可能引用本地或远程资源；实现必须使用IRI检索文件内容。如果实现无法检索存储在远程资源中的文件内容（由于协议不受支持、访问被拒绝或其他问题），它必须发出错误信号。如果没有提供location字段，则必须提供contents字段。实现必须为location字段分配唯一标识符。如果提供了path字段，但没有提供location字段，则实现可以将path字段的值分配给location，然后按照上述规则操作。                                                                                                |
| path           | 可选            | 字符串     | 执行CommandLineTool时，文件可用的本地主机路径。这个字段必须由执行者设置。最后的路径成分必须与basename的值相匹配。这个字段不能在任何其他情况下使用。被执行的命令行工具必须能够使用 POSIX open(2) syscall 访问路径上的文件。作为特殊情况，如果提供了path字段，但location字段没有，则实现可以将path字段的值分配给location，并删除path字段。如果path包含POSIX shell元字符（|，&;，<，>，(，），$，`，\，"，'，<space>，<tab>和<newline>）或应用程序国际化域名不允许的字符，则实现可能会以permanentFailure终止进程。                               |
| basename       | 可选            | 字符串     | 文件的基名称，即没有任何前导目录路径的文件名。基名称不得包含斜杠/。如果没有提供，实现必须在将location解析为IRI后，通过将最终路径组件根据location字段设置此字段。如果提供了basename，则不需要与location的值匹配。当该文件提供给CommandLineTool时，它必须使用basename命名，即path字段的最终组件必须与basename匹配。                                                                                                                                                               |
| dirname        | 可选            | 字符串     | 包含文件的目录的名称，即通往路径中最终斜杠的路径，例如dirname + '/' + basename == path。在评估CommandLineTool文档中的参数引用或表达式之前，实现必须根据path值设置此字段。此字段不得用于任何其他上下文。                                                                                                                                                                                                                              |
| nameroot       | 可选            | 字符串     | 基名称根，如nameroot + nameext == basename，并且nameext为空或以句点开头，最多包含一个句点。为了路径拆分，忽略基名上的前导句；.cshrc的基名将具有.cshrc的命名根。在评估参数引用或表达式之前，实现必须根据basename的值自动设置此字段。                                                                                                                                                                                                             |
| nameext        | 可选            | 字符串     | 基名称扩展名，例如nameroot + nameext == basename，nameext为空或以句点开头，最多包含一个句点。基名上的主要句点将被忽略；.cshrc的基名将有一个空nameext。在评估参数引用或表达式之前，实现必须根据basename的值自动设置此字段。                                                                                                                                                                                                                 |
| checksum       | 可选            | 字符串     | 用于验证文件完整性的可选散列代码。目前必须使用SHA-1算法以“sha1$ +十六进制字符串”的形式出现。                                                                                                                                                                                                                                                                                                      |
| size           | 可选            | long    | 可选文件大小                                                                                                                                                                                                                                                                                                                                                     |
| secondaryFiles | optionalarray | <文件/目录> | 与主文件关联的其他文件或目录的列表，必须与主文件一起传输。示例包括主文件的索引，或加载主文档时必须包含的外部引用。secondaryFiles中列出的文件对象本身可能包括适用相同规则的secondaryFiles                                                                                                                                                                                                                                                 |
<!-- | format         | 可选            | 字符串     | 文件格式：这必须是代表文件格式的概念节点的IRI，最好在本体中定义。如果没有可用的本体，文件格式可以通过完全匹配进行测试。关于格式兼容性的推理必须通过检查输入文件格式是否相同来完成, `owl:equivalentClass` 或者是 `rdfs: subClassOf` 输入参数所要求的格式。 `owl:equivalentClass` 和 `rdfs:subClassOf` 是递归关系, 例如，如果 <B> owl:equivalentClass <C> 和 <B> owl:subclassOf <A> then infer <C> owl:subclassOf <A>.文件格式本体可以在文档根部的“$schemas”元数据中提供。如果$schemas中没有指定本体，运行时可能会执行精确的文件格式匹配 | -->
| contents       | 可选            | 字符串     | 文件内容字面意思。最多64 KiB。如果既不提供location也不提供path，contents必须是非空的。实现必须为location字段分配唯一标识符。当文件作为输入到CommandLineTool时，contents的值必须写入文件。如果inputBinding或outputBinding的loadContents为真，并且location有效，则实现必须从文件中读取最多64 KiB文本并将其放入“内容”字段。                                                                                                                                        |

### 5.1.5.1目录

表示要显示给命令行工具的目录。

目录表示为具有`Directoryclass`的对象。目录对象有许多属性，可以提供有关目录的元数据。目录`location`属性是一个唯一标识目录的URI。实现必须支持file://URI计划，并可能支持http://等其他计划。或者，实现还必须接受目录上的`path`属性，该属性必须是与CWL运行器（用于输入）或命令行工具执行的运行时环境（用于命令行工具输出）在同一主机上可用的文件系统路径。目录对象可能有一个`listing`字段。这是目录中包含的文件和目录对象的列表。对于`listing`的每个条目，`basename`属性在分级到磁盘时定义文件或子目录的名称。如果没有提供`listing`，实现必须有某种方式在运行时根据`location`字段获取目录列表。如果目录没有`location`，则它是目录的字面意思。目录文本必须提供`listing`。必须根据需要在运行时在磁盘上创建目录文本。目录文本中的资源不需要在其`location`有任何隐含的关系。例如，目录列表可能包含位于不同主机上的两个文件。运行时有责任确保将这些文件适当地分级到磁盘。与`listing`的文件关联的辅助文件也必须分阶段到同一目录。执行CommandLineTool时，目录必须先递归阶段，并具有`path`asigend的本地值。CommandLineTool输出中的目录对象必须在工具执行运行时的上下文中提供`location`URI或`path`属性（位于计算节点或执行容器内）。ExpressionTool可以通过使用相同的`location`值将文件引用从输入转发到输出。名称冲突（同一`basename`多次出现在`listing`或列表中的`secondaryFiles`的任何条目中）是一个致命的错误。

**字段**

| 字段       | 必填            | 类型           | 描述                                                                                                                                                                                                                                                                           |
| -------- | ------------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| class    | 必需            | 恒定值Directory | 必须为Directory才能指示此对象描述目录。                                                                                                                                                                                                                                                     |
| location | 可选            | 字符串          | 标识目录资源的IRI。这可能是一个相对的引用，在这种情况下，必须使用文档的基本IRI来解决。该位置可以参考本地或远程资源。如果没有设置listing字段，实现必须使用位置IRI来检索目录列表。如果实现无法检索存储在远程资源的目录列表（由于协议不受支持、访问被拒绝或其他问题），它必须发出错误信号。如果没有提供location字段，则必须提供listing字段。实现必须为location字段分配唯一标识符。如果提供了path字段，但没有提供location字段，则实现可以将path字段的值分配给location，然后按照上述规则操作。 |
| path     | 可选            | 字符串          | 在执行CommandLineTool之前，目录可用的本地路径。这必须由执行者设置。这个字段不能在任何其他情况下使用。正在执行的命令行工具必须能够使用 POSIX opendir(2) syscall 访问路径上的目录。如果path包含POSIX shell元字符（|，&;，<，>，(，），$，`，\，"，'，<space>，<tab>和<newline>）或应用程序国际化域名不允许的字符，则实现可能会以permanentFailure终止进程。                                             |
| basename | 可选            | 字符串          | 目录的基名称，即没有任何前导目录路径的文件名称。基名称不得包含斜杠/。如果没有提供，实现必须在将location解析为IRI后，通过将最终路径组件根据location字段设置此字段。如果提供了basename，则不需要与location的值匹配。当该文件提供给CommandLineTool时，它必须使用basename命名，即path字段的最终组件必须与basename匹配。                                                                                |
| listing  | optionalarray | <文件 \目录>      | 此目录中包含的文件或子目录列表。每个文件或子目录的名称由每个File或Directory对象的basename字段决定。如果File与listing的任何其他条目共享basename为错误。如果两个或多个Directory对象共享相同的basename，则必须将其视为等同于一个子目录，并递归合并列表。                                                                                                                      |

### 5.1.6 CommandInputRecordSchema 

**字段**

| 字段     | 必填            | 类型                                                                   | 描述             |
| ------ | ------------- | -------------------------------------------------------------------- | -------------- |
| type   | 必需            | 恒定值record                                                            | 必须是record      |
| fields | optionalarray | \<CommandInputRecordField> |map<name, type | CommandInputRecordField> | 定义记录字段。        |
| label  | 可选            | 字符串                                                                  | 这个物体的简短、可读的标签。 |
| name   | 可选            | 字符串                                                                  |                |

### 5.1.7 CommandInputRecordField

**字段**

| 字段           | 必填                          | 类型                                                                                                                                                                                      | 描述             |
| ------------ | --------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| name         | 必填                          | 字符串                                                                                                                                                                                     | 字段名称           |
| type         | requiredCWLType             | |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema | string | array<CWLType |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema |string> | 字段类型           |
| doc          | 可选                          | 字符串                                                                                                                                                                                     | 此字段的文档字符串      |
| inputBinding | optional CommandLineBinding |                                                                                                                                                                                         |                |
| label        | 可选                          | 字符串                                                                                                                                                                                     | 这个物体的简短、可读的标签。 |

### 5.1.7.1 CommandInputEnumSchema 

| 字段           | 必填                          | 类型      | 描述             |
| ------------ | --------------------------- | ------- | -------------- |
| symbols      | 必填                          | 数组<字符串> | 定义一组有效符号。      |
| type         | 必需                          | 恒定值enum | 必须是enum        |
| label        | 可选                          | 字符串     | 这个物体的简短、可读的标签。 |
| name         | 可选                          | 字符串     |                |
| inputBinding | optional CommandLineBinding |         |                |

### 5.1.7.2 CommandInputArraySchema 

| 字段           | 必填                          | 类型                                                                                                                                                                                      | 描述             |
| ------------ | --------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| items        | required CWLType            | |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema | string | array<CWLType |CommandInputRecordSchema |CommandInputEnumSchema |CommandInputArraySchema |string> | 定义数组元素的类型。     |
| type         | 必需                          | 恒定值array                                                                                                                                                                                | 必须是array       |
| label        | 可选                          | 字符串                                                                                                                                                                                     | 这个物体的简短、可读的标签。 |
| inputBinding | optional CommandLineBinding |                                                                                                                                                                                         |                |

## **5.2 CommandOutputParameter**

CommandLineTool的输出参数。

| 字段             | 必填                            | 类型                                                                                                                                                                                                             | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| -------------- | ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| id             | 必填                            | 字符串                                                                                                                                                                                                            | 此参数对象的唯一标识符。                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| label          | 可选                            | 字符串                                                                                                                                                                                                            | 这个物体的简短、可读的标签。                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| secondaryFiles | optionalstring                | | 表达式 | array\<string | Expression>                                                                                                                                                                             | 只当 type:File 或是一个items:File列时有效。提供模式或表达式，指定必须与主文件一起包含的文件或目录。所有列出的辅助文件都必须存在。如果不存在预期的辅助文件，则实现可能无法执行工作流程。如果该值是表达式，则表达式中的self值必须是此绑定适用的主输入或输出文件对象。basename、nameroot和nameext字段必须以self存在。ForCommandLineTool输出path字段也必须存在。表达式必须返回相对于主文件路径的文件名字符串、设置path或location和basename字段的文件或目录对象，或由字符串或文件或目录对象组成的数组。将从输入中提取的未更改的文件或目录对象引用为辅助文件是合法的。要处理非文件名保存存储系统，便携式工具描述应避免从location构造新值，而应改用basename或nameroot构建相对引用。如果secondaryFiles中的值不是表达式的字符串，则它指定应将以下模式应用于主文件的路径，以生成相对于主文件的文件名： |
| streamable     | 可选                            | 布尔值                                                                                                                                                                                                            | 只当 type: File 或是一个items: File列时有效.true值表示文件是按顺序读取或写入的，而不寻求。实现可以使用此标志来指示使用命名管道流化文件内容是否有效。默认：false。                                                                                                                                                                                                                                                                                                                                                                  |
| doc            | optionalstring                | | array\<string>                                                                                                                                                                                                | 此类型的文档字符串，或应串联的字符串数组。                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| outputBinding  | optional CommandOutputBinding | 描述如何处理进程的输出。                                                                                                                                                                                                   |                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| format         | optionalstring                | | 表达式                                                                                                                                                                                                          | 只当 type: File 或是一个items: File列时有效.这是分配给输出参数的文件格式。                                                                                                                                                                                                                                                                                                                                                                                                                  |
| type           | optional CWLType              | | stdout | stderr |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string | array<CWLType |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string> | 指定可能分配给此参数的有效数据类型。                                                                                                                                                                                                                                                                                                                                                                                                                                                 |

### 5.2.1 stdout 

仅作为没有`outputBinding`集的`CommandLineTool`输出的`type`有效。

```text
outputs:
  an_output_name:
    type: stdout

stdout: a_stdout_file
```

相当于

```text
outputs:
  an_output_name:
    type: File
    streamable: true
    outputBinding:
      glob: a_stdout_file

stdout: a_stdout_file
```

如果没有提供`stdout`名称，将创建一个随机文件名。例如，以下内容

```text
outputs:
  an_output_name:
    type: stdout
```

相当于

```text
outputs:
  an_output_name:
    type: File
    streamable: true
    outputBinding:
      glob: random_stdout_filenameABCDEFG

stdout: random_stdout_filenameABCDEFG
```

### 符号

| 符号     | 描述  |
| ------ | --- |
| stdout |     |

### 5.2.2 stderr 

仅作为没有`outputBinding`集的`CommandLineTool`输出的`type`有效。

```text
outputs:
  an_output_name:
  type: stderr

stderr: a_stderr_file
```

相当于

```text
outputs:
  an_output_name:
    type: File
    streamable: true
    outputBinding:
      glob: a_stderr_file

stderr: a_stderr_file
```

如果没有提供`stderr`名称，将创建一个随机文件名。例如，以下内容

```text
outputs:
  an_output_name:
    type: stderr
```

相当于

```text
outputs:
  an_output_name:
    type: File
    streamable: true
    outputBinding:
      glob: random_stderr_filenameABCDEFG

stderr: random_stderr_filenameABCDEFG
```

符号

| 符号     | 描述  |
| ------ | --- |
| stdout |     |

### 5.2.3 CommandOutputBinding 

描述如何根据CommandLineTool生成的文件生成输出参数。

输出参数值按以下顺序应用这些操作生成：

- glob

- 加载内容

- 输出Eval

- 次要文件

| 字段           | 必填             | 类型                    | 描述                                                                                                                                                    |
| ------------ | -------------- | --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| glob         | optionalstring | | 表达式 | array\<string> | 使用POSIX glob(3)路径名匹配，查找相对于输出目录的文件。如果提供了一个数组，则查找与数组中任何模式相匹配的文件。如果提供了一个表达式，这个表达式必须返回一个字符串或一个字符串数组，然后将其作为一个或多个glob模式进行计算。必须只匹配并返回实际存在的文件。                                |
| loadContents | 可选             | 布尔值                   | 对于glob中匹配的每个文件，从文件中最多读取前64 KiB文本，并将其放在文件对象contents字段中，以便通过outputEval操作。                                                                               |
| outputEval   | optionalstring | | 表达式                 | 评估表达式以生成输出值。如果指定了glob，self的值必须是包含匹配的文件对象的数组。如果没有匹配文件，self必须是零长度数组；如果匹配单个文件，self的值是单个元素的数组。此外，如果loadContents为true，则文件对象必须在contents字段中包含最多前64 KiB的文件内容。 |

### 5.2.4 CommandOutputRecordSchema 

| 字段     | 必填            | 类型                                                                  | 描述             |
| ------ | ------------- | ------------------------------------------------------------------- | -------------- |
| type   | 必需            | 恒定值record                                                           | 必须是record      |
| fields | optionalarray | \<CommandOutputRecordField> |map<name，type|CommandOutputRecordField> | 定义记录字段。        |
| label  | 可选            | 字符串                                                                 | 这个物体的简短、可读的标签。 |
| name   | 可选            | 字符串                                                                 |                |

### 5.2.5 CommandOutputRecordField

| 字段            | 必填                           | 类型                                                                                                                                                                                           | 描述        |
| ------------- | ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- |
| name          | 必填                           | 字符串                                                                                                                                                                                          | 字段名称      |
| type          | requiredCWLType              | |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string | array<CWLType |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string> | 字段类型      |
| doc           | 可选                           | 字符串                                                                                                                                                                                          | 此字段的文档字符串 |
| outputBinding | optionalCommandOutputBinding |                                                                                                                                                                                              |           |

### 5.2.5.1 CommandOutputEnumSchema 

| 字段            | 必填                           | 类型      | 描述             |
| ------------- | ---------------------------- | ------- | -------------- |
| symbols       | 必填                           | 数组<字符串> | 定义一组有效符号。      |
| type          | 必需                           | 恒定值enum | 必须是enum        |
| label         | 可选                           | 字符串     | 这个物体的简短、可读的标签。 |
| outputBinding | optionalCommandOutputBinding |         |                |

### 5.2.5.2 CommandOutputArraySchema

| 字段            | 必填                           | 类型                                                                                                                                                                                           | 描述             |
| ------------- | ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| items         | requiredCWLType              | |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string | array<CWLType |CommandOutputRecordSchema |CommandOutputEnumSchema |CommandOutputArraySchema |string> | 定义数组元素的类型。     |
| type          | 必需                           | 恒定值enum                                                                                                                                                                                      | 必须是enum        |
| label         | 可选                           | 字符串                                                                                                                                                                                          | 这个物体的简短、可读的标签。 |
| outputBinding | optionalCommandOutputBinding |                                                                                                                                                                                              |                |

## 5.3 内联Javascript要求

指示工作流平台必须支持内联Javascript表达式。如果没有此要求，工作流平台不得执行表达式插值。

| 字段            | 必填            | 类型       | 描述                                        |
| ------------- | ------------- | -------- | ----------------------------------------- |
| class         | 必填            | 字符串      | 始终“内联Javascript要求”                        |
| expressionLib | optionalarray | \<string> | 在执行表达式代码之前也会插入的其他代码片段。允许可以从CWL表达式调用的函数定义。 |

## 5.4 SchemaDef要求

此字段由一系列类型定义组成，在解释`inputs`和`outputs`字段时必须使用这些定义。当`type`字段包含IRI时，实现必须检查类型是否在`schemaDefs`中定义并使用该定义。如果在`schemaDefs`中找不到类型，则为错误。`schemaDefs`中的条目必须按列出的顺序处理，以便以后的模式定义可以参考早期的模式定义。

| 字段    | 必填            | 类型                                                       | 描述         |
| ----- | ------------- | -------------------------------------------------------- | ---------- |
| class | 必填            | 字符串                                                      | 始终'SchemaDefRequirement'|
| types | requiredarray | <InputRecordSchema | InputEnumSchema | InputArraySchema> | 类型定义列表。    |

### 5.4.1 InputRecordSchema 

| 字段     | 必填            | 类型                                        | 描述             |
| ------ | ------------- | ----------------------------------------- | -------------- |
| type   | 必需            | 恒定值record                                 | 必须是record      |
| fields | optionalarray | \<InputRecordField> |map<name，type|输入记录字段> | 定义记录字段。        |
| label  | 可选            | 字符串                                       | 这个物体的简短、可读的标签。 |
| name   | 可选            | 字符串                                       |                |

### 5.4.2 InputRecordField 

| 字段           | 必填                         | 类型                                                                                                                                              | 描述             |
| ------------ | -------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| name         | 必填                         | 字符串                                                                                                                                             | 字段名称           |
| type         | requiredCWLType            | | InputRecordSchema |InputEnumSchema |InputArraySchema | string | array<CWLType |InputRecordSchema |InputEnumSchema |InputArraySchema | string> | 字段类型           |
| doc          | 可选                         | 字符串                                                                                                                                             | 此字段的文档字符串      |
| inputBinding | optionalCommandLineBinding |                                                                                                                                                 |                |
| label        | 可选                         | 字符串                                                                                                                                             | 这个物体的简短、可读的标签。 |

### 5.4.2.1 InputEnumSchema 

| 字段           | 必填                         | 类型      | 描述             |
| ------------ | -------------------------- | ------- | -------------- |
| symbols      | 必填                         | 数组<字符串> | 定义一组有效符号。      |
| type         | 必需                         | 恒定值enum | 必须是enum        |
| label        | 可选                         | 字符串     | 这个物体的简短、可读的标签。 |
| name         | 可选                         | 字符串     |                |
| inputBinding | optionalCommandLineBinding |         |                |

### 5.4.2.2 InputArraySchema 

| 字段           | 必填                         | 类型                                                                                                                                              | 描述             |
| ------------ | -------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| items        | requiredCWLType            | | InputRecordSchema |InputEnumSchema |InputArraySchema | string | array<CWLType |InputRecordSchema |InputEnumSchema |InputArraySchema | string> | 定义数组元素的类型。     |
| type         | 必需                         | 恒定值array                                                                                                                                        | 必须是array       |
| label        | 可选                         | 字符串                                                                                                                                             | 这个物体的简短、可读的标签。 |
| inputBinding | optionalCommandLineBinding |                                                                                                                                                 |                |

5.5 Docker要求

指示应在Docker容器中运行工作流组件，并指定如何获取或构建映像。

如果CommandLineTool在`hints`（或`requirements`）下列出了`DockerRequirement`，它可以（或必须）在指定的Docker容器中运行。平台必须首先获取或安装`dockerPull`、`dockerImport`、`dockerImport`或`dockerFile`指定的正确Docker映像。平台必须在容器中使用`docker run`与适当的Docker镜像和工具命令行执行该工具。工作流平台可以通过使用卷绑定挂载提供输入文件和指定的输出目录。平台应重写输入对象中的文件路径，以对应于Docker绑定挂载位置。也就是说，平台应该重写参数上下文中的值，如`runtime.outdir`、`runtime.tmpdir`和其他值，以成为容器内的有效路径。

运行Docker中包含的工具时，工作流平台不得假设Docker容器的内容，例如特定软件的存在与否，除非假设生成的命令行代表容器运行时环境中的有效命令。

与其他要求的互动

如果[EnvVarRequirement与](https://www.commonwl.org/v1.0/CommandLineTool.html#EnvVarRequirement)DockerRequirement一起指定，则必须使用`--env`或`--env-file`向Docker提供环境变量，并与Docker定义的容器的先前存在环境进行交互。

| 字段                    | 必填  | 类型  | 描述                                                                                      |
| --------------------- | --- | --- | --------------------------------------------------------------------------------------- |
| class                 | 必填  | 字符串 | 始终“Docker要求”                                                                            |
| dockerPull            | 可选  | 字符串 | 指定要使用docker pull取检索的Docker镜像。                                                           |
| dockerLoad            | 可选  | 字符串 | 指定一个HTTP URL，使用docker load从中下载Docker镜像。                                                 |
| dockerFile            | 可选  | 字符串 | 提供将使用docker build构建的Docker文件的内容。                                                        |
| dockerImport          | 可选  | 字符串 | 提供HTTP URL，以使用“docker导入”下载和压缩Docker镜像。                                                  |
| dockerImageId         | 可选  | 字符串 | 用于docker run的镜像ID。可以是人类可读的镜像名称或镜像标识符散列。如果指定了dockerPull，则可以跳过，在这种情况下，必须使用dockerPull镜像ID。 |
| dockerOutputDirectory | 可选  | 字符串 | 将指定的输出目录设置为Docker容器中的特定位置。                                                              |

## 5.6 软件要求

应在定义进程的环境中配置的软件包列表。

| 字段       | 必填            | 类型                                          | 描述        |
| -------- | ------------- | ------------------------------------------- | --------- |
| class    | 必填            | 字符串                                         | 始终“软件要求”  |
| packages | requiredarray | \<SoftwarePackage> |map<package，specs|软件软件包> | 要配置的软件列表。 |

## 5.7 软件包

| 字段      | 必填            | 类型       | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ------- | ------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| package | 必填            | 字符串      | 要提供的软件的名称。如果名称是通用的、不一致的或模糊的，则应与specs字段中的一个或多个标识符组合。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| version | optionalarray | \<string> | 已知兼容的软件的（可选）版本。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| specs   | optionalarray | \<string> | 一个或多个IRI标识用于安装或启用package字段中命名的软件的资源。实现可能会提供解析器，将这些软件标识符IRI映射到某些配置操作；或者他们只能尽最大努力使用package字段的名称。例如，IRI https://packages.debian.org/bowtie可以通过apt-get install bowtie来解决。IRIhttps://anaconda.org/bioconda/bowtie可以通过conda install -c bioconda bowtie来解决。IRI也可以是系统独立的，并用于映射到特定的软件安装或选择机制。以RRID为例：https://identifiers.org/rrid/RRID:SCR_005476可以使用上述Debian或bioconda软件包、Environement Modules管理的本地安装或平台选择的任何其他机制来实现。IRI也可以来自特定学科但仍然依赖系统的标识符来源。例如，与之前的RRID示例相当的ELIXIR工具和数据服务注册表IRI是https://bio.tools/tool/bowtie2/version/2.2.8。如果得到给定注册表的支持，鼓励实现直接查询这些系统独立的软件标识符IRI，以获取与打包系统的链接。也可以列出一个特定站点的IRI。例如，一个使用环境模块的学术计算集群可以列出IRI https://hpc.example.edu/modules/bowtie-tbb/1.22，以表明应该执行模块加载bowtie-tbb/1.1.2，以便在运行附带的工作流或CommandLineTool之前提供用TBB库编译的bowtie 1.1.2版本。请注意，这个例子的IRI是针对特定机构和计算环境的，因为环境模块系统没有一个通用的命名空间或标准化的命名惯例。最后这个示例是最不便携的，只有在基于package字段或更通用的IRI的机制不可用或不合适时才应使用。虽然对其他网站无害，但特定于站点的软件IRI应排除在共享CWL描述之外，以避免杂乱无章。 |

## 5.8 InitialWorkDir要求

定义在执行命令行工具之前，工作流程平台必须在指定的输出目录中创建的文件和子目录列表。

| 字段        | 必填             | 类型    | 描述                                                                                                                                                                                                          |
| --------- | -------------- | ----- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| entry     | requiredstring | | 表达式 | 如果该值是字符串文本或计算为字符串的表达式，则必须以字符串作为文件内容创建新文件。如果该值是计算到File对象的表达式，则表示在执行该工具之前，引用的文件应添加到指定的输出目录中。如果该值是计算到Dirent对象的表达式，则表示entry中的文件或目录应添加到指定的输出目录中，entryname为名称。如果writable为false，可以使用绑定挂载或文件系统链接提供文件，以避免不必要的输入文件复制。 |
| entryname | optionalstring | | 表达式 | 要在输出目录中创建的文件或子目录的名称。如果entry是文件或目录，entryname字段将覆盖文件或目录对象的basename值。可选。                                                                                                                                       |
| writable  | 可选             | 布尔值   | 如果为真，文件或目录必须由工具编写。对文件或目录的更改必须被隔离，并且不能被任何其他CommandLineTool进程看到。这可以通过复制原始文件或目录来实现。默认为false（默认情况下，文件和目录只读）。标记为writable:true目录意味着所有文件和子目录也可以递归写。                                                               |

## 5.9 EnvVar要求

定义将在工具的执行环境中设置的环境变量列表。有关详细信息，请参阅`EnvironmentDef`。

| 字段     | 必填            | 类型                                                        | 描述           |
| ------ | ------------- | --------------------------------------------------------- | ------------ |
| class  | 必填            | 字符串                                                       | 始终“EnvVar要求” |
| envDef | requiredarray | \<EnvironmentDef> |map<envName, envValue | EnvironmentDef> | 环境变量列表。      |

## 5.10 环境变量

定义一个环境变量，该变量将在执行命令行工具时由工作流平台在运行时环境中设置。可能是执行表达式的结果，例如从输入中获取参数。

| 字段       | 必填             | 类型    | 描述     |
| -------- | -------------- | ----- | ------ |
| envName  | 必填             | 字符串   | 环境变量名称 |
| envValue | requiredstring | | 表达式 | 环境变量值  |

## 5.11 ShellCommand要求

修改CommandLineTool的行为，生成一个包含shell命令行的单一字符串。除非CommandLineBinding中包含`shellQuote:false`，否则参数列表中的每一项都必须被加入到一个由单个空格分隔的字符串中，并加引号以防止被shell解释。如果shellQuote:false被指定，参数将被加入到命令字符串中而不加引号，这允许使用shell元字符，例如|用于管道。

| 字段    | 必填  | 类型  | 描述                 |
| ----- | --- | --- | ------------------ |
| class | 必填  | 字符串 | 始终“ShellCommand要求” |

`5.12 所需资源`

指定基本硬件资源要求。

"min "是为安排任务必须保留的最小资源量。如果 "min "不能被满足，该任务不应该被运行。

“最大”是工作允许使用的资源的最大数量。如果一个节点有足够的资源，只要满足每个任务的“最大”资源要求，就可以在单个节点上安排多个任务。如果任务试图超过其“最大”资源分配，实现可能会拒绝额外资源，这可能会导致任务失败。

如果指定了“min”，但没有指定“max”，则“max”==“min”如果“min”没有指定“max”，则“min”==“max”。

如果最大值<最小值，则为错误。

如果这些字段的值为负值，则为错误。

如果资源既没有指定“min”也没有为“max”，则实现可以提供默认值。

| 字段        | 必填           | 类型          | 描述                                 |
| --------- | ------------ | ----------- | ---------------------------------- |
| class     | 必填           | 字符串         | 始终“资源要求”                           |
| coresMin  | optionallong | | 字符串 | 表达式 | 最低保留CPU内核数量                        |
| coresMax  | optionalint  | | 字符串 | 表达式 | CPU内核的最大保留数量                       |
| ramMin    | optionallong | | 字符串 | 表达式 | 最低保留内存（百万字节）（2**20）                |
| ramMax    | optionallong | | 字符串 | 表达式 | 最大保留RAM（百万字节）（2**20）               |
| tmpdirMin | optionallong | | 字符串 | 表达式 | 指定临时目录的最低保留文件系统存储空间，以兆字节为单位（2**20） |
| tmpdirMax | optionallong | | 字符串 | 表达式 | 指定临时目录的最大保留文件系统存储空间，以兆字节为单位（2**20） |
| outdirMin | optionallong | | 字符串 | 表达式 | 指定输出目录的最低保留文件系统存储，以兆字节为单位（2**20）   |
| outdirMax | optionallong | | 字符串 | 表达式 | 指定输出目录的最大保留文件系统存储空间，以兆字节为单位（2**20） |

## 5.13 CWLVersion 

已发布的CWL文档版本的版本符号。

| 符号           | 描述  |
| ------------ | --- |
| draft-2      |     |
| draft-3.dev1 |     |
| draft-3.dev2 |     |
| draft-3.dev3 |     |
| draft-3.dev4 |     |
| draft-3.dev5 |     |
| draft-3      |     |
| draft-4.dev1 |     |
| draft-4.dev2 |     |
| draft-4.dev3 |     |
| v1.0.dev4    |     |
| v1.0         |     |

