# 快速入门指南

## 1. CWL包含哪些元素
一个完整的CWL通常包括以下几个信息：
![aa](../../img/cwl.1.jpg)

### 1.1 CommandLineTool
下面是个详细的示例介绍如何撰写CommandLineTool:
```
cwlVersion: v1.0 # 指定cwl语法版本，目前使用1.0版本
class: CommandLineTool # 指定运行方式为命令行，区别于Workflow

label: "bwa: 序列比对软件" # 简单标签

doc: | # 对该CommandLineTool的详细描述
      原始软件下载地址:
      https://github.com/ebi-pf-team/interproscan/wiki/HowToDownload

      使用教程:
      https://github.com/ebi-pf-team/interproscan/wiki/HowToRun

requirements: # 指定软件的需求和依赖项
  - class: InitialWorkDirRequirement
    listing: [ $(inputs.referenceFile) ] # 此处指定输入文件路径可写，默认cwltool会将输入文件所在目录设置为只读
  EnvVarRequirement: # 指定软件运行特需的环境变量，如没有，此处可省略
    envDef:
        PATH: /bin:/usr/bin:/usr/local/bin

   ResourceRequirement: # 软件运行资源指定，可省略
      ramMin: 10240 # 内存
      coresMin: 3 # cpu核心数

hints:
  DockerRequirement: # 指定docker镜像
    dockerPull: 6oclock/bwa:v0.7.12-r1044

baseCommand: [mem ] # 指定要使用的软件命令

inputs: # 指定输入选项
    core_nums: # 指定输入选项条目名
        type: int?  # 指定参数输入类型为整数，type支持null, boolean, int, long, float, double, string, File, Directory,问号？代表该参数可选
        default: 1 # 设置参数的默认值
        inputBinding: # 指定参数
            prefix: -t # 指定参数名
            separate: true # 指定参数与参数值之间是否分隔，如“-t 4”，有分隔，“-t4”没有分隔，默认为有分隔（此时该选项可省略）
            position: 1 # 指定参数在软件命令中的顺序，此处效果为“bwa mem -t 4”,即紧更着命令mem后出现。
        doc: | # 对该参数的详细介绍
            Set maximum memory per thread; suffix K/M/G recognized [768M]

    markDuplicates: # 指定参数
        type: boolean # 指定参数不需要输入值
        inputBinding: # 指定参数
            position: 2 # 指定参数在软件命令中的位置
            prefix: -M # 指定参数名
    readsHeder: # 指定参数
        type: string
        inputBinding: # 指定参数
            position: 3 # 指定参数在软件命令中的位置
            prefix: -R # 指定参数名

    referenceFile: # 指定输入选项条目名
        type: File # 指定参数输入类型为文件
        secondaryFiles: # 指定更多文件
            - .amb
            - .ann
            - .bwt
            - .pac
            - .sa
        label: Reference sequences in fasta format # 对该输入条目的简短说明，可选
        format: edam:format_1929 # 指定输入文件格式，此处指定引用edam网站fasta格式
        inputBinding: # 指定参数
            position: 4 # 指定参数在软件命令中的位置
        
            
    fastqFile: # 指定输入选项条目名
      type: File # 指定参数输入类型为文件
      label: Raw sequences in fastq format # 对该输入条目的简短说明，可选
      format: edam:format_1930 # 指定输入文件格式，此处指定引用edam网站fastq格式
      inputBinding: # 指定参数
          position: 5 # 指定参数在软件命令中的位置


outputs: # 指定输出选项
  aligned_bam:
    type: stdout # 指定输出到标准输出设备上，同linux系统中的概念
stdout: $(inputs.fastqFile.nameroot).bam # 此处指定标准输出为文件，并定义了文件名
    #format: edam:format_2572 # bam格式
    #outputBinding:
    #  glob: $(inputs.fastqFile.nameroot).bam

$namespaces:
  edam: http://edamontology.org/ # 此处定义类命名空间，相当于一个环境变量，在整个cwl文档范围都可以引用，如在其他地方使用edam，则该变量实际为http://edamontology.org/
 
```

### 2.2 Workflow
Workflow和CommandLineTool大部分语法是相通的，只是Workflow需要额外定义`steps`元素，该信息描述该流程是由哪些CommandLineTool或者子流程组成以及他们之间的依赖关系是怎样的。
```
cwlVersion: v1.0
class: Workflow

inputs: [] #定义输入，此处无输入

outputs:
  classout:
    type: File
    outputSource: compile/compiled_class  # 定义流程最终输出为compile步骤生成的compiled_class

requirements:
  SubworkflowFeatureRequirement: {}

steps:  定义工作流的步骤信息
  compile:  # 步骤命名
    run: 1st-workflow.cwl  # 该步骤的cwl资源文件路径，也可以是cwl内容，此处为文件路径
    in:  #定义该步骤的输入
      tarball: create-tar/tar_compressed_java_file    # create-tar/tar_compressed_java_file表示 变量的值来自步骤create-tar的输出tar_compressed_java_file
      name_of_file_to_extract:  
        default: "Hello.java"    #设置该变量默认值
    out: [compiled_class] #定义该步骤的输出

  create-tar:
    in: []  #定义该步骤的输入
    out: [tar_compressed_java_file]   #定义该步骤的输出
    run: # 该步骤的cwl资源文件路径，也可以是cwl内容，此处为文件内容
      class: CommandLineTool
      requirements:
        InitialWorkDirRequirement: # 步骤运行所需的文件列表
          listing:
            - entryname: Hello.java #文件名称
              entry: |    #文件内容
                public class Hello {
                  public static void main(String[] argv) {
                      System.out.println("Hello from Java");
                  }
                }
      inputs: []  
      baseCommand: [tar, --create, --file=hello.tar, Hello.java]
      outputs:
        tar_compressed_java_file:
          type: File
          streamable: true
          outputBinding:
            glob: "hello.tar"
```