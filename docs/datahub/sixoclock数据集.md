# sixoclock 数据集的使用

sixoclock致力于提供标准的生物医疗健康模拟数据集，以为软件测试，学习和性能评测提供可靠的标准数据集。

您可前往 [数据集](http://www.sixoclock.net/resources/data/) 查找并下载所需数据。

该数据池按数据类型和类别进行存放，目前具有目录结构如下：

```
./
└── NGS
    ├── Homo_sapiens
    │   ├── Others
    │   ├── readme
    │   ├── Reference
    │   ├── RNA_Seq
    │   ├── WES
    │   └── WGS
    └── SARS-COV-2
        ├── readme
        ├── Reference
        └── RNA_Seq

```

您可逐目录查询。