# Sixbox 命令行工具
`Sixbox` 是运行六点了平台应用的工具集合。
`Sixbox-linux` 是`Sixbox`的linux版本，可实现本地linux机器，集群等linux服务器的快速部署和使用。

## 安装

<CodeGroup>
<CodeGroupItem title="Linux 用户" active>

::: tip
  bash 窗口执行命令：
  ```bash
  wget https://bucket.sixoclock.net/resources/dist/latest/Sixbox_linux64_latest.sh
  bash Sixbox_linux64_latest.sh
  ```

  1. 按照安装程序的提示进行操作（如果您不确定任何设置，可以接受默认值）。
  2. 为了使更改生效，请关闭然后重新打开终端窗口。
:::

</CodeGroupItem>
<CodeGroupItem title="类linux系统python 用户" >

```bash
# 下载并安装sixbox
pip3 install sixbox
```

</CodeGroupItem>

<CodeGroupItem title="windows 用户">

::: tip
 1. [安装windows 子系统（WSL2）和docker桌面版](https://docs.docker.com/desktop/windows/wsl/#prerequisites)。
 2. 从微软应用商店[安装 Debian操作系统](https://apps.microsoft.com/store/detail/debian/9MSVKQC78PK6?)。
 3. 在powershell 窗口执行命令`wsl --set-default debian`设置Debian 系统为默认WSL2 子系统
 4. 打开docker desktop，依次选择Settings → Resources → WSL Integration，在 "Enable integration with my WSL distros" 选择 "Debian",。
 5. 在powershell 命令窗口下执行命令（同linux）：
   ```
   wget https://bucket.sixoclock.net/resources/dist/latest/Sixbox_linux64_latest.sh
   bash Sixbox_linux64_latest.sh
   ```
 6. 为了使更改生效，请关闭然后重新打开终端窗口。
:::
</CodeGroupItem>
</CodeGroup>


测试`sixbox`：
```
sixbox -h
sixbox search app bwa
```


## 读写配置文件

`sixbox`包含多个子命令用于实现不同的功能。为了您能够充分使用`sixbox`的全部功能，我们建议您先设置`Sixbox`的配置文件。

`sibox config`命令能够帮助您迅速设置`sixbox`的配置文件。您可以通过`sixbox config -h`来查看`config`命令的相关使用说明。下面向您介绍`config`命令的使用。<br>

### 常规配置
如您需要将从[__sixoclock__](https://www.sixoclock.net/settings/authorize)获取的`客户端登陆授权token`写入配置文件，可使用如下命令：

```bash
sixbox config set token $(token)
```
快速配置channel，可使用如下命令：
```bash
sixbox config add channel $(channel name)
```
如果您想查看`Sixbox`配置文件中的内容，可以使用info命令
```bash
sixbox config info
```
自定义CWLdb的lib路径
```bash
sixbox config set libpath $(libpath)
```

### 客户端登陆秘钥

 如您需要与`sixoclock`进行交互，需要配置客户端登陆秘钥以便`sixbox`访问`sixoclock网站`。

 登陆[`sixoclock` 官网授权中心](http://www.sixoclock.net/settings/authorize) , 点击`新增Token`生成登陆秘钥，复制该秘钥以备用。

  使用sixbox将上述秘钥写入`config.yaml`,
```  
sixbox config set token ${token}
```
## 运行CWL应用
`run`命令用于运行`CWL`应用。可以通过`sixbox run -h`查看参数等帮助信息。运行示例如下:
```bash
sixbox run ./soapnuke-filter.cwl ./soapnuke-filter.yaml
```

如果您没有本地`CWL`应用，可以使用`sixbox`自带的demo应用，亦或使用`pull`命令（有关`pull`命令的使用将在下文进行详细介绍）从sixoclock平台获取相关应用。

运行`本地CWL应用`,
```bash
sixbox run ./soapnuke-filter.cwl./soapnuke-filter.yaml
```

运行`本地CWL任务(cwlc格式)`

```bash
sixbox run ./test.cwlc
```

运行从[sixoclock应用商店](https://www.sixoclock.net/apps)下载的`CWL`应用,
```bash
# 拉取应用
sixbox pull app 5847e32d-127e-469b-9f7a-3e1b97f4625a
# 生成应用的模板参数并手动配置
sixbox run --make-template 6oclock/bowtie2-build:v2.2.9 > ./bowtie2-build.yaml
# 运行指定参数配置下的应用
sixbox run  6oclock/bowtie2-build:v2.2.9 ./bowtie2-build.yaml
```
### 查看运行结果

您可通过`sixbox get job` 或`sixbox get job $(job_id)`查看CWL任务运行情况：
```bash
name                 status        created                 lastUpdated

 test.cwlc-c3tj78     completed     2022-04-29 16:53:24     2022-04-29 16:53:32
 t-3n1PVL             completed     2022-04-29 17:06:33     2022-04-29 17:06:40
 test.cwlc-1mCNFt     completed     2022-04-29 17:47:10     2022-04-29 17:47:17
 test.cwlc-W5JDHS     completed     2022-04-29 17:55:35     2022-04-29 17:55:41
```

查看运行日志`sixbox log`
```bash
sixbox log test.cwlc-c3tj78

```


结果形如：
```
[main] CMD: bwa mem -R @RG\tID:NA12878.1\tLB:NA12878\tSM:NA12878\tPL:ILLUMINA -M -k 19 -t 2 /var/lib/cwl/stg57773f34-98e5-4c15-b6d2-3a482e008953/hg19.chrM.fasta /var/lib/cwl/stg43dc07d5-f534-41d9-8e99-c78c36578687/NA12878.WGS-100K_1.fastq.gz /var/lib/cwl/stgb74b32cb-eb07-4a79-82cf-6d6da84c5535/NA12878.WGS-100K_2.fastq.gz
[main] Real time: 1.211 sec; CPU: 2.296 sec
INFO [job bwa_mem] Max memory used: 0MiB
INFO [job bwa_mem] completed success
INFO Final process status is success
{
    "aligned_sam": {
        "location": "file:///home/6oclock/bwa-mem-aligned.bam",
        "basename": "bwa-mem-aligned.bam",
        "class": "File",
        "checksum": "sha1$8c945faa0aa44753b446b423738623320646d89f",
        "size": 13655009,
        "path": "/home/6oclock/bwa-mem-aligned.bam"
    }
}
```

实时查看运行日志（适用于密集输出型任务）
```
sixbox log -f ${job_name}
```

查看运行输出
```
sixbox log ${job_name} -o outcome
```
其输出如下：
```
{
    "aligned_sam": {
        "location": "file:///home/6oclock/bwa-mem-aligned.bam",
        "basename": "bwa-mem-aligned.bam",
        "class": "File",
        "checksum": "sha1$8c945faa0aa44753b446b423738623320646d89f",
        "size": 13655009,
        "path": "/home/6oclock/bwa-mem-aligned.bam"
    }
}
```
### 停止CWL 任务

`sixbox` 支持停止正在运行的`CWL 任务`，如下：

```
sixbox stop test.cwlc-c3tj78
```

### 指定条件运行
运行结果默认输出在当前目录下，也可以通过`--outdir`指定输出目录：
```bash
sixbox run --outdir /home/test ./soapnuke-filter.cwl ./soapnuke-filter.yaml 
```
其中，
`soapnuke-filter.cwl  `为下载自sixoclock应用中心`soapnuke-filter`软件主文件。
`soapnuke-filter.yaml`为用户配置可视化运行参数的文件。
`resource_id`为您从[sixoclock应用中心](http://www.sixoclock.net/apps)下载的软件主文件的标识`id`，该`id`用于识别区分不同的`CWL`文件。

您也可以使用`--make-template`来生成一个`YAML`格式的参数配置文件，用于手动配置您的软件/应用的参数。
```bash 
sixbox run --make-template ./soapnuke-filter.cwl > ./soapnuke-filter.yaml
```

考虑到您可能不是`docker`用户，`sixbox`提供了`--udocker`和`--singularity`两个参数，帮助您在系统不支持`docker`的情况下，运行`CWL Workflow`
```bash
sixbox run --udocker ./soapnuke-filter.cwl ./soapnuke-filter.yaml 
```

或者，

```bash
sixbox run --singularity ./soapnuke-filter.cwl ./soapnuke-filter.yaml 
```

### 以命令行方式运行CWL 应用
您或许注意到以上的例子中，所输入的`CWL 应用配置文件`并非一个必选项，这是因为`sixbox`同时支持命令行模式配置参数，为您在运行`CWL 应用`的时候提供便利
```bash
 sixbox run 6oclock/bwa:v0.1 -mem test.fastq
```
最后，

`run`命令可以根据`CWLdb`中的`CWL 应用`对应的`tag`或是`resource_id`直接运行`CWLdb`中保存的`CWL 应用`，如下所示：
```bash
sixbox run $(resource_id)
```

或者，

```bash
sixbox run $(tag)
```


## 从应用商店获取CWL应用
### 查找应用
`sixbox` 支持通过命令行从`sixoclock`应用商店查找指定条件的`CWL 应用`

```
sixbox search app bwa
```

```
tag                   provider      name         resource_id                              description

 666oclock/bwa_mem     666oclock     bwa_mem      8a2c930a-32ce-4737-a0c1-2de01ef6182d     bwa_mem 测试案例
```

### 下载应用
`sixbox pull`命令让您可以便捷的从`sixoclock应用中心`中获取您需要的应用：

```bash
sixbox pull app $(pipe_id)
```
其中，`pipe_id`为[sixoclock应用中心](http://www.sixoclock.net/apps)中每个应用的对应`id`，您可点击应用详情页面右侧`sixbox 下载运行`按钮获得下载命令，如图所示：

![image-20220825155059331](../img/image-20220825155059331.png)


## 查看`CWL`应用

您可以使用`get app`查看保存在`$(libPath)/resource/pipe/cwldb/content`目录下的`CWL`应用。
```bash
sixbox get app
```

```
tag                              provider     name              resource_id                              version     
                                                                                                                      
 6oclock/bowtie2-build:v2.2.9     6oclock      bowtie2-build     5847e32d-127e-469b-9f7a-3e1b97f4625a     v2.2.9    
```

查看指定的CWL 应用
```bash
sixbox get app 6oclock/bowtie2-build:v2.2.9
```

查看`CWL 应用`的指定内容`sixbox get app ${app_tag} -o`

以`json格式`导出`CWL 应用`源码,
```
sixbox get app 666oclock/bwa-mem:v1.5 -o json
```


## 提交`CWL`应用至本地`CWLdb`

如果您想将所有的`CWL`应用应用进行统一管理（事实上，我们也建议你这样做，以便于您使用`sixbox`进行统一的操作）。您可以使用`commit app`命令将`CWL`应用提交至本地`CWLdb`中，示例如下：

```text
sixbox commit app ./sixbox-linux/testdata/soapnuke-filter.cwl 666oclock/soapnuke-filter:v1.0
```

`666oclock/soapnuke-filter:v1.0`为该`CWL`应用的`tag`，用于区别其他的`CWL`应用, 其格式为`/`和`:`分隔的字符串。我们建议每个字段的内容是`$(provider)/$(cwl name):$(cwl version)`。

例如示例中，`666oclock`为`CWL`应用的创建者，`soapnuke-filter`为软件的名称，`v1.0`为该`CWL`应用的版本号。

::: tip

组成CWL tag的各字段，支持的字符为`数字`、`英文字母`、`_`、`.`以及`-`组成，且`_`、`.`以及`-`不能为字段的开头或结尾。
:::


当您提交`CWL`应用后,

`CWLdb`下的`metadata`目录和`content`目录中会分别将该`CWL`应用的元信息和源码保存下来，并为这个`CWL`应用分配一个唯一的`id`，以便之后调用该文件。

此外，

如果您所提交的`CWL`应用的`tag`已经存在于`CWLdb`中，`sixbox`将认为这两个文件属于相同文件，并询问您是否需要替换，如果您选择不替换，则本次提交不会进行。

```
root@25fad418f511:/home/test/dist# sixbox commit /home/test/testdata/soapnuke-filter.cwl 666oclock/soapnuke-filter:v1.0
INFO sixbox 3.0.20210124104916
INFO Resolved '/home/test/testdata/soapnuke-filter.cwl' to 'file:///home/test/testdata/soapnuke-filter.cwl'
/home/test/testdata/soapnuke-filter.cwl is valid CWL.
INFO: There is a CWL resource with the same name in CWLdb. PLEASE confirm whether to replace the file
Do you want to overwrite the file? Please input Y/N: 
```

使用`sixbox get app`命令查看相关信息：

```
tag                                provider      name                resource_id                              version     
                                                                                                                           
 6oclock/bowtie2-build:v2.2.9       6oclock       bowtie2-build       5847e32d-127e-469b-9f7a-3e1b97f4625a     v2.2.9      
 666oclock/soapnuke-filter:v1.0     666oclock     soapnuke-filter     deacac36-70de-42e8-8069-3fb15fb715e6     v1.0  
```

可以看到，原本存在的`CWL`应用的相关信息并未发生改变。
如果您选择替换，原有`CWL`应用将会被新提交的文件替换，其`resource_id`也将被改变。

```
tag                                provider      name                resource_id                              version     
                                                                                                                           
 6oclock/bowtie2-build:v2.2.9       6oclock       bowtie2-build       5847e32d-127e-469b-9f7a-3e1b97f4625a     v2.2.9      
 666oclock/soapnuke-filter:v1.0     666oclock     soapnuke-filter     bd9f3559-9047-44a4-bd24-6ea14db48173     v1.0   
```

## 修改CWL 应用的tag
我们提供`sixbox tag`命令，用于您修改`CWLdb`中的`CWL Workflow`相关信息，方便您更好的管理`CWL Workflow`
```bash
sixbox tag app 30479859-c02d-4a40-8e7d-92f9011ac9b8 6oclock/bwa:v0.1
```
其中`30479859-c02d-4a40-8e7d-92f9011ac9b8`为`CWLdb`中`CWL Workflow`对应的`resouece_id`，`6oclock/bwa:v0.1`为`CWL Workflow`新的`tag`信息。

同时，您也可以通过输入原先的`tag`和新的`tag`信息，对`CWL Workflow`的`tag`信息进行修改
```bash
sixbox tag app 6oclock/bwa:v0.1 sixoclock/bwa:v0.2
```

`6oclock/bwa:v0.1`为原先的`tag`信息，`sixoclock/bwa:v0.2`为新的`tag`信息。

## 删除CWLdb中的`CWL 应用`
通过`sixbox rm app`命令，您可以删除`CWLdb`中的`CWL Workflow`，其中输入的参数可以是`CWL Workflow`对应的`resouece_id`或是`tag`
```bash
sixbox rm app 30479859-c02d-4a40-8e7d-92f9011ac9b8
```
其中`30479859-c02d-4a40-8e7d-92f9011ac9b8`为`CWLdb`中`CWL Workflow`对应的`resouece_id`.

您也可以通过如下方式删除`CWLdb`中的`CWL Workflow`
```bash
sixbox rm app 6oclock/bwa:v0.1
```
其中`6oclock/bwa:v0.1`为`CWLdb`中需要删除的`CWL Workflow`对应的`tag`

## 推送CWL 应用到应用商店

`sixbox` 允许将本地`CWLdb`中的`CWL 应用`推送到`sixoclock`应用商店，以供更多人使用。

::: tip

推送应用之前，请先确保您已配置sixbox 客户端授权token，参见[客户端登陆秘钥](#客户端登陆秘钥)

:::


示例如下，
```
sixbox push app 6oclock/bwa:v0.1
```

其中`6oclock/bwa:v0.1`为`CWLdb`中的`CWL 应用`对应的`tag`

## 更新

`sixbox` 提供`update`功能用于在线更新,也可以使用`install`命令安装指定版本的`sixbox`


<CodeGroup>
  <CodeGroupItem title="Linux 用户" active>

```bash
sixbox update sixbox  #更新sixbox到最新版本
sixbox install sixbox=1.1.20210530094243  #安装1.1.20210530094243版本的sixbox
```

  </CodeGroupItem>
  <CodeGroupItem title="python 用户">

```bash
pip3 install --upgrade sixbox  #更新sixbox到最新版本
pip3 install --upgrade sixbox=1.1.20210530094243  #安装1.1.20210530094243版本的sixbox
```

  </CodeGroupItem>
</CodeGroup>

## 关于sixbox的几点说明
### 有关配置文件
在了解`Sixbox`详细用法之前，您需要知道`Sixbox`配置文件的相关信息。

 `Sixbox`的配置文件用于`Sixbox`权限控制、记录[sixoclock应用中心](http://www.sixoclock.net/apps)地址源和`CWLdb`路径，您可以在`${HOME}/.sixbox/config.yaml`中查看。

`Sixbox`配置文件中记载了三类信息：
1. **token**：记录您在`sixoclock`获取的`授权token`。
   
2. **channel**：记录`sixbox`从[sixoclock](http://www.sixoclock.net/apps)中拉取`CWL 应用`的地址。
   
3. **libPath**：记录了`CWLd`b的`lib`路径，`Sixbox`默认lib路径在您安装`Sixbox-linux`的根路径。您也可以通过`config`中相关命令自定义该路径。
   

一个`Sixbox`配置文件的示例：

```
libPath: /root/.sixbox/lib

channel: https://www.sixoclock.net/api

token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzU2NzA0MjYsImlhdCI6MTYzNTY2NjgyNiwidWlkIjoiMTBhOGQ2YzUtZWM4MC00Mzg5LThlYTMtZWYyYTgyY2JlN2M5IiwiaXNfcmVmcmVzaCI6ZmFsc2UsInVuYW1lIjoiNjY2b2Nsb2NrIiwicGVybWlzc2lvbnMiOjIzfQ.ru86vr9cB2hlH-Sgf0i93A9D3_rEX2pk8Zvc1GHaZ
```

### 有关CWLdb
当您在使用`Sixbox`时，`Sixbox`会为您在本地自动建立一个名为`CWLdb`的`CWL Workflow`仓库，以便您管理、运行、查找`CWL Workflow`。

CWLdb的路径默认是在`Sixbox`安装的目录下，但您也可以通过`sixbox config set libPath`命令自定义该路径.

关于`CWLdb`的几点说明：
1. `CWLdb`中设置`metadata目录`用于存放`CWL Workflow`元信息，其主要记录了`CWL Workflow`的作者、名称、版本和`Sixbox`或`sixoclock`为其自动生成的名为`resource_id`的唯一标识码。这些信息有助于您区分、运行、管理`CWL Workflow`。
   
2. `CWLdb`中`content目录`下存放了您通过`sixbox commit app`提交的`CWL Workflow`和您从[sixoclock官方仓库](http://www.sixoclock.net/apps)中下载的`CWL Workflow`。
