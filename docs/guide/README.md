# 介绍

[Sixoclock](https://www.sixoclock.net/) 是一个提供可重用、模块化、可视化生物医疗数据处理算法的云协作平台。通过使用 `sixoclock`，你将快速获取可靠的数据处理`软件`、`流程`，并通过在线的`可视化参数配置界面`完成软件参数的`设置`、软件的`下载`，利用本地计算设备，完成一键下载，一键`运行`。



[Sixoclock](https://www.sixoclock.net/) 诞生的初衷是为了:
* 简化数据分析复杂度，使任何人都可以轻松进行数据分析操作。
* 提高数据分析流程在科研与产业界共享以及重用重现的便利性。



[Sixoclock](https://www.sixoclock.net/) 以[CWL](https://www.commonwl.org)和[docker](https://www.docker.com)为基础。你可以使用 `CWL`来创建内容（如`工具`、`流程`等）， `sixoclock` 会帮助你生成可视化页面来`展示`、`设置`、`运行`它们。你也可以基于[sixoclock社区](https://www.sixoclock.net/apps)进行算法的托管，拖拽智能组合，以及在线计算和用户业务交付。


<iframe src="https://player.bilibili.com/player.html?bvid=1Zv411N7ru&page=1" scrolling="no" width="100%" height="600px" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
<div align='center' > 一分钟了解sixoclock </div>





## 它是如何工作的？

从技术上讲，[Sixoclock](https://www.sixoclock.net/) 是一个以`CWL`和`docker`为基础的数据处理流程/数据集/文档托管、协作与计算平台。

[Sixoclock](https://www.sixoclock.net/)里所有工具/流程都由`CWL`构成，`CWL`替代了传统的`shell`、`perl`等流程脚本，并在可拓展性和规范性上优于`WDL`。`CWL` 旨在满足数据密集型科学的需求，例如生物信息学、医学成像、天文学、物理学和化学。相比`perl`，`shell`等语言，`CWL`更容易，你可以轻松掌握`CWL`。



**开发者**（你、我或sixoclock生信团队）将数据处理流程/软件所有运行时依赖封装到`docker`，然后拟写一个流程描述文档（在`sixoclock`平台是`CWL`），发布到[sixoclock应用仓库](https://www.sixoclock.net/apps)，`sixoclock`负责对`CWL`进行可视化和计算环境自适应。



**用户**在[Sixoclock](https://www.sixoclock.net/)查找所需软件，通过可视化界面配置软件运行参数，下载到本地，使用[sixbox](../clients/sixbox-linux.md)客户端一键运行，[sixbox](../clients/sixbox-linux.md)自动解析流程依赖并安装，依据配置好的参数执行计算任务。你也可以使用任何`CWL`解释器运行下载自`sixoclock`的流程。



## 为什么不是 ...?

### shell脚本

`shell`是`linux` 自带的编程语言，诚然，`shell`可以做很多杰出的工作，但在生物医疗等数据密集型和复杂性应用场景下，`shell`过度自由化以及无法很好的处理算法依赖，流程输入输出接口，使得`shell`脚本共享给他人使用时的门槛较高，其算法可重现性较差，对于复杂的流程来说，用户在新的环境安装依赖也是一件很有挑战的事情，开发者维护也是一件不容易的事情。

### 只用Docker

`Docker`是一个轻量运行时容器环境。你可以将你的`开发环境`、`代码`、`配置文件`等一并打包到这个容器中，并发布和应用到任意平台中。`sixoclock`本身也使用了`Docker`来解决流程运行依赖环境的一件安装，但生物医疗数据计算是构建在算法基础上的，以工具的使用和数据挖掘为目的，`Docker`是一个优秀依赖管理工具，但仅用`docker`不足以解决流程计算问题。

### WDL

`WDL`同样是一款优秀的流程描述语言，与`CWL`可以做类似的事情，但是`WDL`语法不够严格，拓展性较差，在构建复杂流程时可能难以胜任，不过，相比`CWL`, `WDL`语法较为简单，国内部分企业也在内部使用。

