# 快速上手

## 依赖环境

- [sixbox](https://www.sixoclock.net/download-center)
- [Docker](https://www.docker.com)/[udocker](https://indigo-dc.gitbook.io/udocker)/[singularity](https://sylabs.io/singularity) （任选其一或由`sixbox`来自动安装）

::: tip
- 容器运行环境由 `sixbox` 自动安装的策略是，先检查用户是否安装 `docker` 并具有运行权限，然后依次是 `singularity` 和 `udocker` ，若都没有，则自动安装 `udocker`。
- 容器运行环境使用 [docker](https://www.docker.com) 时，你需要具有root权限或者 docker 运行权限。
- 容器运行环境使用 [singularity](https://sylabs.io/singularity)时，你需要 singularity 运行权限 。
:::


## 下载客户端软件sixbox


运行`sixoclock平台`的应用，需要您的系统中安装有sixoclock客户端软件`sixbox`，您可前往 [下载中心](https://www.sixoclock.net/download-center) 下载对应版本安装程序。

`sixbox`基于docker(需要root或docker运行权限)或 udocker（不需要root权限，但部分软件无法正常运行）。


<CodeGroup>
<CodeGroupItem title="Linux 用户" active>

```bash
wget https://bucket.sixoclock.net/resources/dist/latest/Sixbox_linux64_latest.sh
bash Sixbox_linux64_latest.sh
```

    1. 按照安装程序屏幕上的提示进行操作（如果您不确定任何设置，可以接受默认值）。
    2. 为了使更改生效，请关闭然后重新打开终端窗口。
    3. 测试您的安装。在终端窗口，运行`sixbox -h`命令。如果已正确安装，将显示参数说明。

</CodeGroupItem>
<CodeGroupItem title="python 用户" >

```bash
# 下载并安装sixbox
pip3 install sixbox
```

</CodeGroupItem>

</CodeGroup>

## 下载与运行应用


### 命令行操作

1. 查找应用
```bash
sixbox search app samtools-faidx
```

2. 下载应用到本地
```bash
sixbox pull app f6683d3d-4a00-4c64-a299-15c8cfca1be8
```

3. 配置应用

为应用运行指定参数配置，如配置`samtools-faidx` 应用参数为：
```bash
input_f:  # type "File"
    class: File
    path: http://bucket.sixoclock.net/resources/data/NGS/Homo_sapiens/Reference/hg19.chrM.fasta
```
即配置参数`input_f` 为`http://bucket.sixoclock.net/resources/data/NGS/Homo_sapiens/Reference/hg19.chrM.fasta`

使用命令`sixbox run --make-template app_tag`可生成应用参数模板文件，如本例：
```
sixbox run --make-template 6oclock/samtools-faidx:v1.11 > samtools-faidx.job.yaml
```
可将模板参数写入文件`samtools-faidx.job.yaml`中，稍后你可修改并配置该文件。

4. 运行应用

```
sixbox run 6oclock/samtools-faidx:v1.11 samtools-faidx.job.yaml
```

其中， 

`samtools-faidx.cwl` 为 下载的 `samtools-faidx` 应用文件，

`samtools-faidx.job.yaml` 为运行参数。

5. 查看结果

应用成功运行将会生成输出文件路径汇总结果和运行日志。

以linux `bash`运行为例（其它平台类似），最终结果为：


```bash
INFO [job samtools-faidx] Max memory used: 0MiB
INFO [job samtools-faidx] completed success
{
    "fa_with_fai": {
        "location": "file:///home/6oclock/hg19.chrM.fasta",
        "basename": "hg19.chrM.fasta",
        "class": "File",
        "checksum": "sha1$e0dd15b14c54f5e8825471d5757123c8262e3d18",
        "size": 16909,
        "secondaryFiles": [
            {
                "basename": "hg19.chrM.fasta.fai",
                "location": "file:///home/6oclock/hg19.chrM.fasta.fai",
                "class": "File",
                "checksum": "sha1$0b67f58f9156444807fb14ede65da6d1c0025705",
                "size": 19,
                "path": "/home/6oclock/hg19.chrM.fasta.fai"
            }
        ],
        "path": "/home/6oclock/hg19.chrM.fasta"
    },
    "fai": {
        "location": "file:///home/6oclock/hg19.chrM.fasta.fai",
        "basename": "hg19.chrM.fasta.fai",
        "class": "File",
        "checksum": "sha1$0b67f58f9156444807fb14ede65da6d1c0025705",
        "size": 19,
        "path": "/home/6oclock/hg19.chrM.fasta.fai"      
    }
}
INFO Final process status is success
```

### 网页操作


登陆并访问 [应用中心](https://www.sixoclock.net/apps) ，搜索需要的应用。

![image-appsotres](../img/查找应用.gif)

点击所选应用，进入应用详情页，
详情页提供应用基本信息，应用配置等信息。其中，



`应用` 面板提供应用结构查看与配置功能，您可点击下方应用结构图中的图标进行配置。



&nbsp;

页面右上角 `下载` 按钮提供了该应用的下载，提供json与yaml两种格式,您可任选一种下载。



以应用 `samtools-faidx` 对参考基因组索引为例，可通过两种方式配置应用：

#### 参数源码模式

导入`yaml`格式的参数配置信息以运行应用，

![image-parameter_settings](../img/应用参数运行.gif)

#### 可视化点击模式

点击 `input_f` 图标，设置参考基因组路径，

用户可前往下载 [hg19.chr21.fasta](https://bucket.sixoclock.net/resources/data/NGS/Homo_sapiens/Reference/hg19.chr21.fasta)以作测试用，

如本例，上传文件 完成该参数的配置。

![image-parameter_settings](../img/上传文件运行.gif)



至此，一个完整的数据处理过程结束了，您可以开始尝试处理自己的数据。快去[sixoclock网站](https://www.sixoclock.net/) 开启可视化数据分析之路吧。



![](../img/查找网站.gif)


## 发布第一个应用
六点了是一个公开的平台，您可以将自己的应用发布到平台，从而让更多的人使用你的应用。

六点了以CWL为标准应用开发语言，任何遵循CWL1.0规范的应用都可以通过客户端软件sixbox(详情参见[sixbox 安装](#下载客户端软件sixbox))发布到平台，下面以一个简单例子做说明：

一个helloword应用：
```
cwlVersion: v1.0 # 指定cwl语法版本，目前使用1.0版本
class: CommandLineTool # 指定运行方式为命令行，区别于Workflow

id: zhangsan  #  应用的id，在流程可视化图中区分流程模块，合法字符为英文字符和下划线
label: "张三: 单细胞小能手" # 简单标签

doc: | # 对该CommandLineTool的详细描述
      这是我的第一个应用


requirements: # 指定软件的需求和依赖项
   ResourceRequirement: # 软件运行资源指定，可省略
      ramMin: 524 # 内存
      coresMin: 1 # cpu核心数

hints:
  DockerRequirement: # 指定docker镜像，一般用于指定封装实际应用程序的docker镜像（需要是一个公开课访问的镜像地址）
    dockerPull: alpine

baseCommand: [echo ] # 指定要使用的镜像内软件命令

inputs: # 指定输入选项
    words: # 指定输入选项条目名
        type: string?  # 指定参数输入类型为整数，type支持null, boolean, int, long, float, double, string, File, Directory,问号？代表该参数可选
        default: 张三说李四笑王五不知sixoclock # 设置参数的默认值
        inputBinding: # 指定参数
            prefix: -e # 指定参数名
            separate: true # 指定参数与参数值之间是否分隔，如“-t 4”，有分隔，“-t4”没有分隔，默认为有分隔（此时该选项可省略）
            position: 1 # 指定参数在软件命令中的顺序，此处效果为“bwa mem -t 4”,即紧更着命令mem后出现。
        doc: | # 对该参数的详细介绍
            指定想说的话



outputs: # 指定输出选项
  helloword:
    type: stdout # 指定输出到标准输出设备上，同linux系统中的概念
stdout: helloword.txt # 此处指定标准输出重定向到文件，并定义了文件名，同linux中的'>'符号的作用

```

将该上述CWL 源码写入文件`helloword.cwl`

通过[sixbox](../clients/sixbox-linux.md) 提交到本地仓库：
```
sixbox commit app ./helloword.cwl sixoclock/helloword:v1.0
```
其中`sixoclock`为作者用户名，`helloword`为应用名，`v1.0`为应用的版本

检验是否可以运行：
```
sixbox run sixoclock/helloword:v1.0
```

发布到六点了平台（发布前请先确保已经配置[sixbox登陆秘钥](../clients/sixbox-linux.md#客户端登陆秘钥)）
```
sixbox push app sixoclock/helloword:v1.0
```

进阶应用开发教程可阅读[CWL 中文教程](../dev_guide/CWL/introduction.md)和[sixbox使用教程](../clients/sixbox-linux.md)。

现在您可以在我的应用找到刚才发布的应用.