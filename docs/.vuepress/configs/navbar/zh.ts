import type { NavbarConfig } from '@vuepress/theme-default'
// import { version } from '../meta'

export const zh: NavbarConfig = [
  {
    text: '首页',
    link: 'https://www.sixoclock.net/',
  },

  {
    text: '入门',
    link: '/guide/',
  },

  {
  text: '开发者手册',
  children: [
    {
      text: 'CWL中文教程',
      children: [
        '/dev_guide/CWL/introduction.md',
        '/dev_guide/CWL/quick-start-guide.md',
        '/dev_guide/CWL/cwl-user_guide-commandline.md',
        '/dev_guide/CWL/cwl-user_guide-workflow.md',
        '/dev_guide/CWL/CommandlineTool-description-specification.md',
        '/dev_guide/CWL/workflow-description-specification.md',
      ],
    },

    {
      text: 'data Schema 规范',
      children: [
        '/dev_guide/dataSchemas/data_schema.md',
      ],
    },
  ]
},

  {
  text: '客户端',
  children: [
    {
      text: 'sixbox',
      children: [
        '/clients/sixbox-linux.md',
        // '/clients/sixbox-windows.md',
      ],
    },
    {
      text: 'python cli',
      children: [
        '/clients/sixrunr.md'
      ],
    },
  ]
},

  {
  text: '应用案例',
  children: [
    {
      text: '文章复现',
      children: [
        '/cases/coronavirus_re-emerged.md',
      ],
    },

    {
      text: '开发者自研算法',
      children: [
        // '/cases/sepsis-cox.md'
      ],
    },

  ]
},

{
  text: '意见反馈',
  link: '/Advice.md',
},

{
  text: '常见Q&A',
  link: '/Q_A.md',
},
{
  text: '关于我们',
    children: [
      '/about/Whoweare.md',
      '/about/Follow_us.md',
      '/about/Contact_us.md',
      '/about/Join_us.md',
  ],
},
  // {
  //   text: '参考',
  //   children: [
  //     {
  //       text: 'VuePress',
  //       children: [
  //         '/zh/reference/cli.md',
  //         '/zh/reference/config.md',
  //         '/zh/reference/frontmatter.md',
  //         '/zh/reference/components.md',
  //         '/zh/reference/plugin-api.md',
  //         '/zh/reference/theme-api.md',
  //         '/zh/reference/client-api.md',
  //         '/zh/reference/node-api.md',
  //       ],
  //     }, 
  //     {
  //       text: '客户端',
  //       children: [
  //         '/clients/ixbox-linux.md',
  //         '/clients/sixbox-windows.md',
  //       ],
  //     },
  //     {
  //       text: '默认主题',
  //       children: [
  //         '/zh/reference/default-theme/config.md',
  //         '/zh/reference/default-theme/frontmatter.md',
  //         '/zh/reference/default-theme/components.md',
  //         '/zh/reference/default-theme/markdown.md',
  //         '/zh/reference/default-theme/styles.md',
  //       ],
  //     },
  //   ],
  // },
  // {
  //   text: '插件',
  //   children: [
  //     {
  //       text: '常用功能',
  //       children: [
  //         '/zh/reference/plugin/back-to-top.md',
  //         '/zh/reference/plugin/container.md',
  //         '/zh/reference/plugin/google-analytics.md',
  //         '/zh/reference/plugin/medium-zoom.md',
  //         '/zh/reference/plugin/nprogress.md',
  //         '/zh/reference/plugin/register-components.md',
  //       ],
  //     },
  //     {
  //       text: '内容搜索',
  //       children: [
  //         '/zh/reference/plugin/docsearch.md',
  //         '/zh/reference/plugin/search.md',
  //       ],
  //     },
  //     {
  //       text: 'PWA',
  //       children: [
  //         '/zh/reference/plugin/pwa.md',
  //         '/zh/reference/plugin/pwa-popup.md',
  //       ],
  //     },
  //     {
  //       text: '语法高亮',
  //       children: [
  //         '/zh/reference/plugin/prismjs.md',
  //         '/zh/reference/plugin/shiki.md',
  //       ],
  //     },
  //     {
  //       text: '主题开发',
  //       children: [
  //         '/zh/reference/plugin/active-header-links.md',
  //         '/zh/reference/plugin/debug.md',
  //         '/zh/reference/plugin/git.md',
  //         '/zh/reference/plugin/palette.md',
  //         '/zh/reference/plugin/theme-data.md',
  //         '/zh/reference/plugin/toc.md',
  //       ],
  //     },
  //   ],
  // },
  // {
  //   text: '了解更多',
  //   children: [
  //     {
  //       text: '深入',
  //       children: [
  //         '/zh/advanced/architecture.md',
  //         '/zh/advanced/plugin.md',
  //         '/zh/advanced/theme.md',
  //         {
  //           text: 'Cookbook',
  //           link: '/zh/advanced/cookbook/',
  //         },
  //       ],
  //     },
  //     {
  //       text: '其他资源',
  //       children: [
  //         '/zh/contributing.md',
  //         {
  //           text: 'Awesome VuePress',
  //           link: 'https://github.com/vuepress/awesome-vuepress',
  //         },
  //       ],
  //     },
  //   ],
  // },

  // {
  //   text: "更新日志",
  //   children: [
  //     {
  //       text: 'sixbox for linux',
  //       link:
  //         'https://github.com/vuepress/vuepress-next/blob/main/CHANGELOG.md',
  //     },
  //     {
  //       text: 'sixbox for windows',
  //       link: 'https://v1.vuepress.vuejs.org/zh/',
  //     },
  //     {
  //       text: 'sixrunr',
  //       link: 'https://v0.vuepress.vuejs.org/zh/',
  //     },
  //   ],
  // },
]
