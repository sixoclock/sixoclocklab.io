import { defineUserConfig } from '@vuepress/cli'
import type { DefaultThemeOptions } from '@vuepress/theme-default'
import { navbar, sidebar } from './configs'




export default defineUserConfig<DefaultThemeOptions>({
    lang: 'zh-CN',
    title: 'sixoclock 帮助文档',
    description: '六点了协作云是一个专业的提供生物医疗数据、算法、知识等内容在线创作的平台。',
    head: [
      ['link', { rel: 'icon', href: '/favicon.ico' }],
      [
        'script',
        { src: 'https://hm.baidu.com/hm.js?44212d6ce872df50b804d94b24889284' },
      ],
      [
        'meta',
        {
          name: 'keywords',
          content: '六点了,sixoclock,帮助文档',
        },
      ],
      [
        'meta',
        {
          name: 'og:url',
          content: 'https://www.sixoclock.net/',
        },
      ],
    ],

    themeConfig: {
      logo: '/images/logo-black.png',
      logoDark: '/images/logo-white.png',
      home: "/README.md",
      backToHome: "返回主页",

      contributors: false,
      base: '/',
      // docsDir: 'docs',

      // page meta
      editLinkText: '在 GitHub 上编辑此页',
      lastUpdatedText: '上次更新',
      contributorsText: '贡献者',

      // custom containers
      tip: '提示',
      warning: '注意',
      danger: '警告',

      // 404 page
      notFound: [
        '这里什么都没有',
        '我们怎么到这来了？',
        '这是一个 404 页面',
        '看起来我们进入了错误的链接',
      ],

      // github 仓库
      docsRepo: 'https://github.com/6-oclock/six-docs',
      docsBranch: 'main',
      docsDir: 'docs',
      editLinkPattern: ':repo/edit/:branch/:path',
      // a11y
      openInNewWindow: '在新窗口打开',
      toggleDarkMode: '切换夜间模式',
      toggleSidebar: '切换侧边栏',

      navbar: navbar.zh,
      sidebar: sidebar.zh,
      // [
      //   // NavbarItem

      //   {
      //       text: '入门教程',
      //       children: ['/coronavirus_re-emerged.md', '/clients/sixbox-windows.md'],
      //     },

 
      //   // NavbarGroup
      //   {
      //     text: '客户端',
      //     children: ['/clients/sixbox-linux.md', '/clients/sixbox-windows.md'],
      //   },
      //   // 字符串 - 页面文件路径
      //   {
      //       text: '应用案例',
      //       children: ['/coronavirus_re-emerged.md', '/clients/sixbox-windows.md'],
      //     },
      //   {
      //   text: '意见建议',
      //   link: '/Advice.md',
      //   },
      //   {
      //   text: '常见Q&A',
      //   link: '/Q_A.md',
      //   },
      // ],
      
    },

    plugins: [
        [
          '@vuepress/plugin-search',
          {
            locales: {
              '/': {
                placeholder: '搜索',
              },
              '/zh/': {
                placeholder: '搜索',
              },
            },
          },
        ],
      ],
  })