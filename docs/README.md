---
home: true
title: 首页
heroImage: /images/logo-black.png
actions:
  - text: 快速上手
    link: /guide/start_sixoclock.html
    type: primary
  - text: sixoclock简介
    link: /guide/
    type: secondary
features:
  - title: 简洁至上
    details: 以 CWL 为基础的生物医疗数据处理协作，以最少的配置帮助你专注于数据分析而不是环境配置。
  - title: 基于Docker
    details: 享受 Docker带来的一键配置软件安装环境，即启即用的生命周期管理。
  - title: 可视化
    details: sixoclock会将每一个CWL流程预渲染生成可视化页面，你可以用来展示、设置、运行它们。

footer: 为生物医疗健康提供最便捷的数据挖掘云平台 | Copyright © 2022 六点了科技
---

### 人人都可快速上手

<CodeGroup>
  <CodeGroupItem title="Linux 用户" active>

```bash
# 下载并安装sixbox
wget https://bucket.sixoclock.net/resources/dist/latest/Sixbox_linux64_latest.sh

bash Sixbox_linux64_latest.sh

# 搜索在线应用
sixbox search app bowtie
# 拉取在线应用到本地
sixbox pull app 5847e32d-127e-469b-9f7a-3e1b97f4625a
# 生成应用的模板参数并手动配置
sixbox run --make-template 6oclock/bowtie2-build:v2.2.9 > ./bowtie2-build.yaml
# 运行指定参数配置下的应用
sixbox run  6oclock/bowtie2-build:v2.2.9 ./bowtie2-build.yaml


```

  </CodeGroupItem>

  <CodeGroupItem title="类Linux系统python 用户">

```bash
# 下载并安装sixbox
pip3 install sixbox

# 搜索在线应用`bowtie`
sixbox search app bowtie
# 拉取在线应用到本地
sixbox pull app 5847e32d-127e-469b-9f7a-3e1b97f4625a
# 生成应用的模板参数并手动配置
sixbox run --make-template 6oclock/bowtie2-build:v2.2.9 > ./bowtie2-build.yaml
# 运行指定参数配置下的应用
sixbox run  6oclock/bowtie2-build:v2.2.9 ./bowtie2-build.yaml
```

  </CodeGroupItem>
</CodeGroup>
