# 脓毒症预后算法


## 查找算法

先去sixoclock网站找到你要运行的算法，比如我们找算法`sepsis-cox`, 登陆[sixoclock](https://www.sixoclock.net/)网站，在网站搜索框输入sepsis-cox，回车，查看结果：

![image-20210824175941822](../img/image-20210824175941822.png)

![image-20210824175837913](../img/image-20210824175837913.png)

点击搜索到的结果，进入`详情页`，你可以阅读相关条目了解算法的更详细介绍

![image-20210824180045803](../img/image-20210824180045803.png)

点击右上角`设置运行`按钮设置算法的参数，

![image-20210824180433615](../img/image-20210824180433615.png)



双击黄色图标，在data那里填入数据路径，找到你要处理的数据在你自己电脑上的路径，比如我的电脑上是：

`C:\Users\6oclock\Documents\WeChat Files\wxid_ryji0tms8s0a22\FileStorage\File\2021-08\ICU_sepsis_3days_withOStime_processed_data_clean73feat_demo.csv`，

填入方框内,

![image-20210824180705448](../img/image-20210824180705448.png)



再看看蓝色图标里有没有规定其它要设置的参数，

![image-20210824180901489](../img/\image-20210824180901489.png)



参数设置完，点击下载，	

![image-20210824181102629](../img/image-20210824181102629.png)



打开下载的压缩包，把它解压到你认为合适的位置：

![image-20210824181240503](../img/image-20210824181240503.png)



![image-20210824181452638](../img/image-20210824181452638.png)



## 使用sixbox运行算法

上一步下载好了需要的算法，并用在线网站可视化配置好了参数，现在开始运行算法，

打开[sixbox](../clients/sixbox-windows.md), 如您没有安装sixbox，请前往[下载中心](https://www.sixoclock.net/download-center)下载

![image-20210824181739797](../img/image-20210824181739797.png)

依次指定`CWL`，`YML`，保存目录，点击运行开始运行算法

![image-20210824182301580](../img/image-20210824182301580.png)



![image-20210824182439037](../img/image-20210824182439037.png)



到输出路径，查看结果，

![image-20210824182642028](../img/image-20210824182642028.png)

打开结果，由于这里我们是用的一个脓毒症预后算法，按照算法作者的定义，输出结果是一个包含两列数据csv文件，

![image-20210824182958170](../img/image-20210824182958170.png)



**参考：**

[sepsis-cox](https://www.sixoclock.net/application/pipe/413322a0-1a3f-49de-af33-134edca80d12/)