# 常见问题与解决

## 1. 如何运行下载自sixoclock平台的算法

sixoclock平台所有算法采用基于[docker][1]的[cwl][2]文件进行算法的描述，因此所有您下载的算法本质上是一个cwl文件，通过配置与cwl相对应的yml参数文件，您将可以自定义运行该算法，sixoclock提供了本地运行cwl的工具[sixbox][3],您可下载对应操作系统版本。




## 2. 软件仓库里的工具, 流程, 工作, 工作流是怎么定义的？各代表什么？

对生物医疗等数据密集型分析任务而言，一个具体的分析任务往往由多个软件（算法）按一定顺序依次处理原始输入数据，其中既包括对单个软件的选择与配置，也包括多个软件如何组合以及组合后的参数如何适配，为了扩大sixoclock平台的适用性，我们将生信计算中涉及的这些操作进行抽象，划分出4个概念：

工具：处理单个任务的软件/算法，接收输入与参数配置，处理后返回输出结果，其本质是一个软件的参数结构描述文件([cwl][2])，在`六点了`平台，是一个基于docker的cwl，如`bwa mem`即为一个工具。

工具流：处理一个复杂任务的一系列相互关联的部分有序的工具集合，其中一个工具的输出可能是另一个工具的输入，而其他的工具可能是相互独立的。接收一系列输入与参数配置，处理后返回一系列输出结果，比如一个典型的`人类重测序call 变异流程`。

工作：对CWL`工具`的参数配置，是一个为完成特定个性化分析而对某个`工具`配置的参数，`工作`基于`工具`,是`工具`的个性化配置，其本质是一个包含CWL源码的参数配置文件，一般以cwlc格式表示。

工作流：与`工作`类似，是对CWL`工具流`的参数配置，`工作流`基于`工具流`,是`工具流`的个性化配置，其本质也是一个参数配置文件，，一般以cwlc格式表示。

## 3. sixbox pull报错提示`may require 'login'`
sixbox pull 功能需要登录，sixbox依据用户存储在本地的sixoclock客户端授权token（可通过sixbox config info命令查看）访问sixoclock平台，如您没有生成对应token，请前往网站http://www.sixoclock.net/application/u-center/authorize 生成token，并使用命令sixbox set token <刚刚生成的token>来设置token。若本地已存在token，仍报错，则可能是现有token已经过期，请登录网站重新生成token。

## 4. 如何更新sixbox
执行命令sixbox update sixbox更新本地sixbox为最新版本。

## 5. 系统没有安装docker，还可以运行sixbox吗？
sixbox同时支持docker，udocker，singularity三种容器运行后端，只要任何一种软件可以在您的工作环境中正常运行，sixbox即可正确解析CWL内指定的镜像


[1]: https://www.docker.com/
[2]: https://www.commonwl.org/
[3]: http://www.sixoclock.net/download-center
